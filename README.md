# HEXIS DATA

## Clone the project

    mkdir  demo && cd  demo
    git clone git clone https://maganapro@bitbucket.org/magana/d201812.git ./
    composer update

## Database

Shell:
`mysql -u root -p`

MySQL shell:

    CREATE DATABASE magana_demo;
    CREATE USER 'magana_demo'@'localhost' IDENTIFIED BY 'magana_demo';
    GRANT ALL PRIVILEGES ON magana_demo.* TO 'magana_demo'@'localhost';
    exit;

Shell:
php bin/console doctrine:migration:diff
php bin/console doctrine:migration:migrate
One the database is created you should open your browser and go to:

    //LOCALHOST/**sync**

> NOTE: This task needs time so you need to increase the execution time (Ex. on nginx `fastcgi_read_timeout 600s;`)

> Now the application must be running... navigate to the root of the hte project `//LOCALHOST/`.

## Root applications

- Sync database from ERPs `//LOCALHOST/sync/`
- Sync offer status by french offer : `//LOCALHOST/sync/status`
- Commercial offers : `//LOCALHOST/explore`
- Export data in CSV, JSON or XML : `//LOCALHOST/export/`
- Export for PIM : `//LOCALHOST/pim/`
- Catalogue : `//LOCALHOST/`
  - Catalogue uses the browser history and cookies so you can navigate between categories. Ex. `//LOCALHOST/#/cat/100/0001/00010001/2/`

---

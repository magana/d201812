var last_query = {
  dos: null,
  lv1: null,
  lv2: null,
  lv3: null
};
var catalogue_var = {
  dos: null,
  cat: null,
  sub: null,
  gamme: null
};

var $el = jQuery("#catalogue-content");
var $lv1 = jQuery(".cat.lv1");
var $lv2 = jQuery(".cat.lv2");
var $lv3 = jQuery(".cat.lv3");

jQuery("#drop-lang a").click(function () {
  let dos = jQuery(this).data("lang");
  catalogue_var.dos = dos;
  setTimeout(function () {
    jQuery.router.set("#/cat/" + dos + "/0/0/null/");
    jQuery("#lang")
      .val(dos)
      .change();
  }, 100);
});

jQuery(document).on("change", "#lang", function () {
  let dos = jQuery(this).val();
  Cookies.set("hex_lang", dos);
  jQuery.router.set("#/cat/" + dos + "/0/0/null/");
});

jQuery(document).on("change", "#lv1", function () {
  let parent = jQuery(this).val();
  jQuery.router.set("#/cat/" + catalogue_var.dos + "/" + parent + "/0/null/");
});
jQuery(document).on("change", "#lv2", function () {
  let parent = jQuery(this).val();
  jQuery.router.set(
    "#/cat/" +
    catalogue_var.dos +
    "/" +
    catalogue_var.cat +
    "/" +
    parent +
    "/null/"
  );
});

jQuery(document).on("change", "#lv3", function () {
  let gamme = jQuery(this).val();
  jQuery.router.set(
    "#/cat/" +
    catalogue_var.dos +
    "/" +
    catalogue_var.cat +
    "/" +
    catalogue_var.sub +
    "/" +
    gamme +
    "/"
  );
});

function getLocale(dos) {
  let codes = {
    0: "en",
    100: "fr",
    200: "de",
    300: "it",
    400: "es",
    500: "en"
  };
  return codes[dos];
}

function loadImages() {
  let cat = catalogue_var.sub != 0 ? catalogue_var.sub : catalogue_var.cat;
  let url = `/${getLocale(Cookies.get("hex_lang"))}/parts/image-grid/${
    catalogue_var.dos
  }/${cat}/${catalogue_var.gamme}`;
  jQuery(".download").html(
    `<a class="btn btn-primary btn-block" href="/download/${
      catalogue_var.mode
    }/${catalogue_var.dos}/${catalogue_var.cat}/${
      catalogue_var.gamme
    }">Download ZIP</a>`
  );
  jQuery.ajax({
    url: url,
    dataType: "html",
    method: "GET",
    success: function (data) {
      $el.html(data);
      jQuery(window).lazyLoadXT();
      $el.lazyLoadXT({
        visibleOnly: false,
        checkDuplicates: false
      });
      jQuery(window).trigger("loadComplete");
    },
    error: function (response) {
      $el.html("<h1>Error loading the category</h1>");
    }
  });
}

function loadHome() {
  let cat = catalogue_var.sub != 0 ? catalogue_var.sub : catalogue_var.cat;
  let url = `/${getLocale(Cookies.get("hex_lang"))}/parts/home-category/${
    catalogue_var.dos
  }`;
  jQuery(".download").html("");
  jQuery.ajax({
    url: url,
    dataType: "html",
    method: "GET",
    success: function (data) {
      $el.html(data);
      jQuery(window).trigger("loadComplete");
    },
    error: function (response) {
      $el.html("<h1>Error loading the category tree</h1>");
    }
  });
}

function loadDropdown(url, selector, value) {
  jQuery.ajax({
    url: url,
    dataType: "html",
    method: "GET",
    success: function (data) {
      jQuery(".cat." + selector).html(data);
      jQuery("#" + selector).val(value);
      jQuery(window).trigger("resize");
    },
    error: function (response) {
      $el.html("<h1>Error loading the category</h1>");
    }
  });
}

jQuery.route("/cat/:dos/:lv1/:lv2/:lv3", function (data, params) {
  if (params.dos == 0) {
    let lang = jQuery("#lng-select").html();
    $el.html(`<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
      <h1 class="display-4">Choose your language</h1>
      <p class="lead col-12 col-md-3 mx-auto">${lang}</p>
    </div>
  </div>`);
    $lv1.html("");
    jQuery(window).trigger("loadComplete");

    return;
  }
  let url;
  let page = "home";
  if (last_query.dos != params.dos) {
    jQuery("#lang").val(params.dos);
    catalogue_var.dos = params.dos;
    catalogue_var.cat = params.lv1;
    catalogue_var.sub = params.lv2;
    catalogue_var.gamme = params.lv3;
    catalogue_var.mode = "images"; /*TODO: add function for csv and images */
    url = `/${getLocale(
      Cookies.get("hex_lang")
    )}/parts/dw-category/${Cookies.get("hex_lang")}`;
    loadDropdown(url, "lv1", params.lv1);
    $lv2.html("");
    $lv3.html("");
    console.log("lang", page, last_query.lv1, params.lv1);
  }

  if (last_query.lv1 != params.lv1) {
    jQuery("#lv1").val(params.lv1);
    url = `/${getLocale(Cookies.get("hex_lang"))}/parts/dw-subcategory/${
      catalogue_var.dos
    }/${params.lv1}`;
    catalogue_var.cat = params.lv1;
    catalogue_var.sub = params.lv2;
    $lv2.html("");
    $lv3.html("");
    if (params.lv1 != 0) {
      page = "catalogue";
      console.log("----lv1", page, params);
      loadDropdown(url, "lv2", params.lv2);
    }
  }
  if (params.lv1 != 0 || last_query.lv2 != params.lv2) {
    catalogue_var.sub = params.lv2;
    url = `/${getLocale(Cookies.get("hex_lang"))}/parts/dw-gammes/${params.dos}/${params.lv2}`;
    if (params.lv2 != 0) {
      page = "catalogue";
      console.log("----lv2", page, params);
      loadDropdown(url, "lv3", params.lv3);
    }
  }

  if (page == 'catalogue' && params.lv3 != "null") {
    page = "catalogue";
    console.log("----lv3", page, params);
    jQuery("#lv3").val(params.lv3);
  }
  console.log("---------------", page, params);
  catalogue_var.gamme = params.lv3;
  if (page == "catalogue") {
    loadImages();
  } else {
    loadHome();
  }
  last_query = params;
});

function spaCatalogue() {
  jQuery.router.init();
  let dos = Cookies.get("hex_lang");
  if (window.location.hash.substring(2, 5) == "") {
    if (dos) {
      jQuery.router.set("#/cat/" + dos + "/0/0/null");
    } else {
      jQuery(window).trigger("loadComplete");
    }
  }
}
spaCatalogue();
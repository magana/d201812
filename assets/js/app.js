jQuery("#lang").change(function () {
  let dos = jQuery(this).val();
  Cookies.set('hex_lang', dos);
  jQuery(".main-container").hide();
  location.reload();
});


jQuery(document).on('click', '[data-toggle="lightbox"]', function (event) {
  event.preventDefault();
  jQuery(this).ekkoLightbox();
});

jQuery(window).on("loadComplete", function () {
  jQuery('#loading').hide();
  jQuery('#main-content').show();
});

jQuery(document).ready(function () {
  jQuery("#lang").val(Cookies.get('hex_lang'));
});
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/assets/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./assets/js/catalogue.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/js/catalogue.js":
/*!********************************!*\
  !*** ./assets/js/catalogue.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var last_query = {
  dos: null,
  lv1: null,
  lv2: null,
  lv3: null
};
var catalogue_var = {
  dos: null,
  cat: null,
  sub: null,
  gamme: null
};

var $el = jQuery("#catalogue-content");
var $lv1 = jQuery(".cat.lv1");
var $lv2 = jQuery(".cat.lv2");
var $lv3 = jQuery(".cat.lv3");

jQuery("#drop-lang a").click(function () {
  var dos = jQuery(this).data("lang");
  catalogue_var.dos = dos;
  setTimeout(function () {
    jQuery.router.set("#/cat/" + dos + "/0/0/null/");
    jQuery("#lang").val(dos).change();
  }, 100);
});

jQuery(document).on("change", "#lang", function () {
  var dos = jQuery(this).val();
  Cookies.set("hex_lang", dos);
  jQuery.router.set("#/cat/" + dos + "/0/0/null/");
});

jQuery(document).on("change", "#lv1", function () {
  var parent = jQuery(this).val();
  jQuery.router.set("#/cat/" + catalogue_var.dos + "/" + parent + "/0/null/");
});
jQuery(document).on("change", "#lv2", function () {
  var parent = jQuery(this).val();
  jQuery.router.set("#/cat/" + catalogue_var.dos + "/" + catalogue_var.cat + "/" + parent + "/null/");
});

jQuery(document).on("change", "#lv3", function () {
  var gamme = jQuery(this).val();
  jQuery.router.set("#/cat/" + catalogue_var.dos + "/" + catalogue_var.cat + "/" + catalogue_var.sub + "/" + gamme + "/");
});

function getLocale(dos) {
  var codes = {
    0: "en",
    100: "fr",
    200: "de",
    300: "it",
    400: "es",
    500: "en"
  };
  return codes[dos];
}

function loadImages() {
  var cat = catalogue_var.sub != 0 ? catalogue_var.sub : catalogue_var.cat;
  var url = "/" + getLocale(Cookies.get("hex_lang")) + "/parts/image-grid/" + catalogue_var.dos + "/" + cat + "/" + catalogue_var.gamme;
  jQuery(".download").html("<a class=\"btn btn-primary btn-block\" href=\"/download/" + catalogue_var.mode + "/" + catalogue_var.dos + "/" + catalogue_var.cat + "/" + catalogue_var.gamme + "\">Download ZIP</a>");
  jQuery.ajax({
    url: url,
    dataType: "html",
    method: "GET",
    success: function success(data) {
      $el.html(data);
      jQuery(window).lazyLoadXT();
      $el.lazyLoadXT({
        visibleOnly: false,
        checkDuplicates: false
      });
      jQuery(window).trigger("loadComplete");
    },
    error: function error(response) {
      $el.html("<h1>Error loading the category</h1>");
    }
  });
}

function loadHome() {
  var cat = catalogue_var.sub != 0 ? catalogue_var.sub : catalogue_var.cat;
  var url = "/" + getLocale(Cookies.get("hex_lang")) + "/parts/home-category/" + catalogue_var.dos;
  jQuery(".download").html("");
  jQuery.ajax({
    url: url,
    dataType: "html",
    method: "GET",
    success: function success(data) {
      $el.html(data);
      jQuery(window).trigger("loadComplete");
    },
    error: function error(response) {
      $el.html("<h1>Error loading the category tree</h1>");
    }
  });
}

function loadDropdown(url, selector, value) {
  jQuery.ajax({
    url: url,
    dataType: "html",
    method: "GET",
    success: function success(data) {
      jQuery(".cat." + selector).html(data);
      jQuery("#" + selector).val(value);
      jQuery(window).trigger("resize");
    },
    error: function error(response) {
      $el.html("<h1>Error loading the category</h1>");
    }
  });
}

jQuery.route("/cat/:dos/:lv1/:lv2/:lv3", function (data, params) {
  if (params.dos == 0) {
    var lang = jQuery("#lng-select").html();
    $el.html("<div class=\"jumbotron jumbotron-fluid\">\n    <div class=\"container text-center\">\n      <h1 class=\"display-4\">Choose your language</h1>\n      <p class=\"lead col-12 col-md-3 mx-auto\">" + lang + "</p>\n    </div>\n  </div>");
    $lv1.html("");
    jQuery(window).trigger("loadComplete");

    return;
  }
  var url = void 0;
  var page = "home";
  if (last_query.dos != params.dos) {
    jQuery("#lang").val(params.dos);
    catalogue_var.dos = params.dos;
    catalogue_var.cat = params.lv1;
    catalogue_var.sub = params.lv2;
    catalogue_var.gamme = params.lv3;
    catalogue_var.mode = "images"; /*TODO: add function for csv and images */
    url = "/" + getLocale(Cookies.get("hex_lang")) + "/parts/dw-category/" + Cookies.get("hex_lang");
    loadDropdown(url, "lv1", params.lv1);
    $lv2.html("");
    $lv3.html("");
    console.log("lang", page, last_query.lv1, params.lv1);
  }

  if (last_query.lv1 != params.lv1) {
    jQuery("#lv1").val(params.lv1);
    url = "/" + getLocale(Cookies.get("hex_lang")) + "/parts/dw-subcategory/" + catalogue_var.dos + "/" + params.lv1;
    catalogue_var.cat = params.lv1;
    catalogue_var.sub = params.lv2;
    $lv2.html("");
    $lv3.html("");
    if (params.lv1 != 0) {
      page = "catalogue";
      console.log("----lv1", page, params);
      loadDropdown(url, "lv2", params.lv2);
    }
  }
  if (params.lv1 != 0 || last_query.lv2 != params.lv2) {
    catalogue_var.sub = params.lv2;
    url = "/" + getLocale(Cookies.get("hex_lang")) + "/parts/dw-gammes/" + params.dos + "/" + params.lv2;
    if (params.lv2 != 0) {
      page = "catalogue";
      console.log("----lv2", page, params);
      loadDropdown(url, "lv3", params.lv3);
    }
  }

  if (page == 'catalogue' && params.lv3 != "null") {
    page = "catalogue";
    console.log("----lv3", page, params);
    jQuery("#lv3").val(params.lv3);
  }
  console.log("---------------", page, params);
  catalogue_var.gamme = params.lv3;
  if (page == "catalogue") {
    loadImages();
  } else {
    loadHome();
  }
  last_query = params;
});

function spaCatalogue() {
  jQuery.router.init();
  var dos = Cookies.get("hex_lang");
  if (window.location.hash.substring(2, 5) == "") {
    if (dos) {
      jQuery.router.set("#/cat/" + dos + "/0/0/null");
    } else {
      jQuery(window).trigger("loadComplete");
    }
  }
}
spaCatalogue();

/***/ })

/******/ });
//# sourceMappingURL=catalogue.js.map
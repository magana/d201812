document.addEventListener("DOMContentLoaded", function (event) {

    var classname = document.getElementsByClassName("toggle");
    for (var i = 0; i < classname.length; i++) {
        classname[i].addEventListener('click', function (event) {
            event.preventDefault();
            let tab = event.target.hash;
            var content = document.getElementById(tab.replace("#", ""));
            if (!content) return;
            toggle(content);
        }, false);
    }
    var h4Menu = document.getElementsByClassName("toggle-ul");
    console.log(h4Menu);
    for (var i = 0; i < h4Menu.length; i++) {
        h4Menu[i].addEventListener('touchstart', navMobile, false);
        h4Menu[i].addEventListener('click', navMobile, false);
    }

    [].forEach.call(document.getElementsByClassName("iframe-lightbox-link"), function (el) {
        el.lightbox = new IframeLightbox(el);
    });
});
var lastUl = null;
var navMobile = function (event) {
    event.preventDefault();
    let ul = this.parentNode
    if (lastUl) hide(lastUl);
    toggle(ul);
    if (lastUl == ul) hide(ul);
    lastUl = ul;
};
var show = function (elem) {
    console.log(elem)
    elem.classList.add('is-visible');
};

// Hide an element
var hide = function (elem) {
    elem.classList.remove('is-visible');
};

// Toggle element visibility
var toggle = function (elem) {
    elem.classList.toggle('is-visible');
};
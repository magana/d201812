<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\HexArticles;
use App\Entity\HexArticlesTree;
use App\Entity\HexFamilyRates;
use App\Entity\HexFieldsI18n;
use App\Hexis\Tools\LangOptions;
use App\Hexis\Tools\WPFuncs;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Hexis\Tools\Locale;

/**
* @Route("/explore")
*/
class ExploreController extends AbstractController
{
    /**
     * @Route("/{_locale}", name="explore_index")
     */
    public function index(WPFuncs $wp,Request $request,\Twig_Environment $twig)
    {
        $em = $this->getDoctrine()->getManager();
        $locale = new Locale($request);
        $cookie = $locale->getCookie();
        $html =  $twig->render('explore/index.html.twig', [
            'is_logegged_id' => $wp->isLoggedIn(),
            'art_dos' => LangOptions::options($em),
            'topheader' => file_get_contents("{$request->getScheme()}://{$request->getHttpHost()}/mu-site/header/". $request->getLocale()),
        ]);
        $response = new Response( $html );
        $response->headers->setCookie($cookie);
        return $response;      
        
    }
    /**
     * @Route("/offers/{dos}", name="explore_offers")
     */
    public function offers($dos='all', WPFuncs $wp, Request $request,\Twig_Environment $twig)
    {
        ini_set('memory_limit', '-1');
        $em = $this->getDoctrine()->getManager();
        $repo_arts = $em->getRepository(HexArticles::class);
        $arts = $repo_arts->getArtsWithTree($dos);
       
        $tmp_array = [];
        foreach ($arts as $art) {
            $gamme = "G_{$art['Gamme']}_{$art['erp_id']}";
            $tmp_array[] = [
                'id' =>  $art['id'],
                'dos' => $dos,
                'category' => $art['TreeName1'],
                'subcategory' => $art['TreeName2'],
                'gamme' => $gamme,
                'art' => $art['art'],
                'article' =>  $art['article'],
            ];
        }
        $locale = new Locale($request);
        $cookie = $locale->getCookie();
        $html =  $twig->render('explore/offers.html.twig', [
            'is_logegged_id' => $wp->isLoggedIn(),
            'art_dos' => LangOptions::options($em),
            'arts' => $tmp_array,
            'topheader' => file_get_contents("{$request->getScheme()}://{$request->getHttpHost()}/mu-site/header/". $request->getLocale()),
        ]);
        $response = new Response($html);
        $response->headers->setCookie($cookie);
        return $response;       
    }
     /**
     * @Route("/compare/", name="explore_compare")
     */
    public function compare( WPFuncs $wp, Request $request,\Twig_Environment $twig)
    {
        ini_set('memory_limit', '-1');
        $em = $this->getDoctrine()->getManager();
        $repo_arts = $em->getRepository(HexArticles::class);
        $all_dos = LangOptions::options($em);
        $temp_offers=[];
        foreach ($all_dos as $dos) {
            $arts = $repo_arts->getArtsWithTree($dos['dos']);
            foreach ($arts as $art) {
                $gamme = "G_{$art['Gamme']}_{$art['erp_id']}";
                if (!isset($temp_offers[$art['art']])) {
                    $temp_offers[$art['art']]=[
                        'category' => $art['TreeName1'],
                        'subcategory' => $art['TreeName2'],
                        'gamme' => $gamme,
                        'art' => $art['art'],
                        $dos['dos'] => 'X',
                    ];                    
                }else{
                    $temp_offers[$art['art']][$dos['dos']]= "X";
                }
            }
        }
        $locale = new Locale($request);
        $cookie = $locale->getCookie();
        $html =  $twig->render('explore/compare.html.twig', [
            'is_logegged_id' => $wp->isLoggedIn(),
            'art_dos' => LangOptions::options($em),
            'arts' => $temp_offers,
            'topheader' => file_get_contents("{$request->getScheme()}://{$request->getHttpHost()}/mu-site/header/". $request->getLocale()),
        ]);
        $response = new Response($html);
        $response->headers->setCookie($cookie);
        return $response;       
    }
}
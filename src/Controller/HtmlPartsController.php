<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\HexArticles;
use App\Entity\HexArticlesTree;
use Gumlet\ImageResize;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\TranslatorInterface;

/**
* @Route("/{_locale}/parts", defaults={"_locale": "en"})
*/
class HtmlPartsController extends AbstractController
{
    /**
     * @Route("/home-category/{dos}", name="html_parts_home_cats")
     */
    public function homeCategories(Request $request,$dos,TranslatorInterface $translator)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(HexArticlesTree::class);
        $cats = $repo->getFirstLevelByDos($dos);
        $repo_arts = $em->getRepository(HexArticles::class);   
        foreach ($cats as $key => $cat) {
            $arts=$repo_arts->getArtByCriteria($cat['dos'], $cat['erp_id'], 'null');
            $cats[$key]['count'] = count($arts);
            shuffle($arts);
            $cats[$key]['imgs'] = [];
            $k = 0;
            foreach ($arts as $k => $art) {
                $src = $art->getArt();
                $thumb = realpath('../') . "/public/docs/products/thumbs/{$src}.jpg";
                if(file_exists($thumb)){
                    $src = "/docs/products/thumbs/{$src}.jpg";
                }else{
                    $src = "/assets/img/no-image.png";
                }
                $cats[$key]['imgs'][] = ['src' => $src, 'alt' => $art->getDes()];
                if($k==2){
                    break;
                }
            }
        }
        return $this->render('html_parts/cats-home.html.twig', [
            'cats' => $cats,
            'dos' => $dos,
        ]);
    }
    /**
     * @Route("/dw-category/{dos}", name="html_parts_dw_catagory")
     */
    public function categories(Request $request,$dos,TranslatorInterface $translator)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(HexArticlesTree::class);
        //echo "<pre style='position:relative;z-index:9999'>";var_dump($dos, __LINE__, 'Run on: ' . __FILE__ . ' > Line: ' . __LINE__);echo '</pre>';die;
        $cats = $repo->getFirstLevelByDos($dos);
        $repo_arts = $em->getRepository(HexArticles::class);   
        foreach ($cats as $key => $cat) {
            $cats[$key]['count'] = count($repo_arts->getArtByCriteria($cat['dos'], $cat['erp_id'], 'null'));
        }
        return $this->render('html_parts/dw-level1.html.twig', [
            'cats' => $cats,
        ]);
    }
    /**
     * @Route("/dw-subcategory/{dos}/{parent}", name="html_parts_dw_subcategory")
     */
    public function subcategory($dos, $parent)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(HexArticlesTree::class);
        $cats = $repo->getSecondLevelByDos($parent, $dos);
        $repo_arts = $em->getRepository(HexArticles::class);       
         foreach ($cats as $key => $cat) {
            $cats[$key]['count'] = count($repo_arts->getArtByCriteria($cat['dos'], $cat['erp_id'], 'null'));
        }
        return $this->render('html_parts/dw-level2.html.twig', [
            'cats' => $cats,
        ]);
    }
    /**
     * @Route("/dw-gammes/{dos}/{parent}", name="html_parts_dw_gammes")
     */
    public function gammes($dos,$parent)
    {
          $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(HexArticles::class);
        $cats = $repo->getGameLevelByDos($parent);
        $repo_arts = $em->getRepository(HexArticles::class);  
        foreach ($cats as $key => $cat) {
            $cats[$key]['count'] = count($repo_arts->getArtByCriteria($dos, $parent, $cat['gamme'])) ?: 0;
            $cats[$key]['name'] = "Range {$cats[$key]['gamme']}";
            if($cats[$key]['count']==0){
                unset($cats[$key]);
            }else{
                if ($cats[$key]['gamme'] == 0) {
                    $cats[$key]['name'] = 'Out of range';
                    $tmp = $cats[$key];
                    unset($cats[$key]);
                    $cats[]= $tmp;
                }
            }
        }
        return $this->render('html_parts/dw-level3.html.twig', [
            'cats' => $cats,
        ]);
    }
    /**
     * @Route("/image-grid/{dos}/{parent}/{gamme}", name="html_parts_image_grids")
     */
    public function imageGrid($dos, $parent=null, $gamme=null)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(HexArticles::class);
        $arts = $repo->getArtByCriteria($dos, $parent, $gamme);
        $imgs=[];
        foreach ($arts as $art) {
            $img = $art->getArt();
            $des = $art->getDes();
            $img_src = realpath('../') . "/public/docs/products/{$img}.jpg";
        
            if (file_exists($img_src)) {
                $thumb = realpath('../') . "/public/docs/products/thumbs/{$img}.jpg";
                if (!file_exists($thumb)) {
                    $image = new ImageResize($img_src);
                    $image->resize(150, 150);
                    $image->save($thumb);
                }
                $imgs[] = [
                    'thumb' => "/docs/products/thumbs/{$img}.jpg",
                    'src' => "/docs/products/{$img}.jpg",
                    'name' => $img,
                    'des' => $des,
                ];
            }else{
                $imgs[] = [
                    'thumb' => "/assets/img/no-image.png",
                    'src' => "/assets/img/no-image.png",
                    'name' => $img,
                    'des' => $des,
                ];
            }
        }
        if(strlen($parent)==1){
            shuffle($imgs);
        }

        return $this->render('html_parts/image-grid.html.twig', [
            'imgs' => $imgs,
        ]);
    }
    
}
<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\HexArticles;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Hexis\Tools\WPFuncs;
use App\Hexis\Tools\LangOptions;
use App\Hexis\Tools\Locale;


class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(Request $request, \Twig_Environment $twig, WPFuncs $wp)
    {
        
        $em = $this->getDoctrine()->getManager();
        $locale = new Locale($request);
        $cookie = $locale->getCookie();
        $html =  $twig->render('index/index.html.twig', [
            'is_logegged_id' => $wp->isLoggedIn(),
            'art_dos' => LangOptions::options($em),
            'topheader' => file_get_contents("{$request->getScheme()}://{$request->getHttpHost()}/mu-site/header/". $request->getLocale()),
        ]);
        $response = new Response($html);
        $response->headers->setCookie($cookie);
        return $response;
    }
}
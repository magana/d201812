<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\HexArticles;
use App\Entity\HexArticlesTree;
use App\Entity\HexFamilyRates;
use Symfony\Component\HttpFoundation\Response;
use App\Hexis\Tools\Responses;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use App\Entity\HexFieldsI18n;
use App\Hexis\Tools\WPFuncs;
use App\Hexis\Tools\LangOptions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;
use App\Hexis\Tools\Locale;

/**
* @Route("/export")
*/
class ExportController extends AbstractController
{
    /**
     * @Route("/", name="export_index")
     */
    public function index(WPFuncs $wp, Request $request, \Twig_Environment $twig)
    {
        $em = $this->getDoctrine()->getManager();

        $repo_arts = $em->getRepository(HexArticles::class);
  
        $repo_cats = $em->getRepository(HexArticlesTree::class);
        $cats_dos = $repo_cats->getNbTreeByDos();
        
        $repo_rates = $em->getRepository(HexFamilyRates::class);
        $rates_dos = $repo_rates->getNbRateByDos();

        $locale = new Locale($request);
        $cookie = $locale->getCookie();
        $html =  $twig->render('export/index.html.twig', [
            'is_logegged_id' => $wp->isLoggedIn(),
            'art_dos' => LangOptions::options($em),
            'cats_dos' => $cats_dos,
            'rates_dos' => $rates_dos,
            'topheader' => file_get_contents("{$request->getScheme()}://{$request->getHttpHost()}/mu-site/header/". $request->getLocale()),
        ]);
        $response = new Response($html);
        $response->headers->setCookie($cookie);
        return $response;       
       
    }
    /**
     * @Route("/articles/{format}/{dos}", name="export_arts")
     */
    public function articles($format='csv', $dos='all')
    {
        ini_set('memory_limit', '-1');
        if ($dos=='all') {
            $cretaria = [];
        } else {
            $cretaria = ['dos' => $dos];
        }
        $cats_repo = $this->getDoctrine()
            ->getRepository(HexArticles::class);
        $cats = $cats_repo->findBy($cretaria);

        $tmp_arts = [];
        foreach ($cats as $cat) {
            $tmp_arts[] = [
                'id' =>  $cat->getId(),
                'article' =>  $cat->getArticle(),
                'dos' => $cat->getDos(),
                'ref' => $cat->getRef(),
                'art' => $cat->getArt(),
                'status' => $cat->getStatus(),
                'date_created' => $cat->getDateCreated(),
                'date_updated' => $cat->getDateUpdated(),
                'gamme' => $cat->getGamme(),
                'type' => $cat->getType(),
                'dim0001' => $cat->getDim0001(),
                'dim0002' => $cat->getDim0002(),
                'poid_unitaire' => $cat->getPoidUnitaire(),
                'poid_brut' => $cat->getPoidBrut(),
                'poid_net' => $cat->getPoidNet(),
                'ean' => $cat->getEan(),
                'dim_unitaire' => $cat->getDimUnitaire(),
                'des' => $cat->getDes(),
                'familly_rate_id' => $cat->getFamilyRate() ? $cat->getFamilyRate()->getId() : null,
                'category_id' => $cat->getCategory() ? $cat->getCategory()->getId() : null,
            ];
        }
        return Responses::download($tmp_arts, 'articles', $dos, $format);
    }
    /**
     * @Route("/categories/{format}/{dos}", name="export_cats")
     */
    public function categories($format='csv', $dos='all')
    {
        $cats_repo = $this->getDoctrine()
            ->getRepository(HexArticlesTree::class);
        $cats = $cats_repo->getTreeWithDos($dos);
        $tmp_cats = [];
        
        foreach ($cats as $cat) {
            if ($dos != 'all') {
                $labels = $cat->getLabel();
                foreach ($labels as $label) {
                    if ($label->getDos() == $dos) {
                        break;
                    }
                }
                $label = $label->getValue();
                $tmp_cats[] = [
                'id' =>  $cat->getId(),
                'place' => $cat->getPlace(),
                'parent' => $cat->getParent(),
                'erp_id' => $cat->getErpId(),
                'label' => $label
            ];
            } else {
                $tmp_cats[] = [
                'id' =>  $cat['id'],
                'place' => $cat['place'],
                'parent' => $cat['parent'],
                'erp_id' => $cat['erp_id'],
                'label' => $cat['value'],
                'dos' => $cat['dos']
                ];
            }
        }
        return Responses::download($tmp_cats, 'categories', $dos, $format);
    }
    /**
     * @Route("/rates/{format}/{dos}", name="export_rates")
     */
    public function rates($format='csv', $dos='all')
    {
        $rates_repo = $this->getDoctrine()
            ->getRepository(HexFamilyRates::class);
        $rates = $rates_repo->getRatesWithDos($dos);
        $tmp_rates = [];
        
        foreach ($rates as $rate) {
            if ($dos != 'all') {
                $labels = $rate->getLabel();
                foreach ($labels as $label) {
                    if ($label->getDos() == $dos) {
                        break;
                    }
                }
                $label = $label->getValue();
                $tmp_rates[] = [
                'id' =>  $rate->getId(),
                'erp_id' => $rate->getErpId(),
                'label' => $label
            ];
            } else {
                $tmp_rates[] = [
                'id' =>  $rate['id'],
                'erp_id' => $rate['erp_id'],
                'label' => $rate['value'],
                'dos' => $rate['dos']
                ];
            }
        }
        return Responses::download($tmp_rates, 'rates', $dos, $format);
    }
    /**
     * @Route("/offers/{format}/{dos}", name="export_offers")
     */
    public function offers($format='csv', $dos='all')
    {
        ini_set('memory_limit', '-1');
        if ($dos=='all') {
            $cretaria = [];
        } else {
            $cretaria = ['dos' => $dos];
        }
        $cats_repo = $this->getDoctrine()
            ->getRepository(HexArticles::class);
        $i18n_repo = $this->getDoctrine()
            ->getRepository(HexFieldsI18n::class);
        $tree_repo = $this->getDoctrine()
            ->getRepository(HexArticlesTree::class);
            
        $cats = $cats_repo->findBy($cretaria);

        $tmp_arts = [];
        foreach ($cats as $cat) {
            $cat2 = null;
            $cat1 = $cat->getCategory();
            if ($cat1) {
                $cat_ref = $cat1;
                $cat1 = $i18n_repo->findOneBy(['tree_label' => $cat_ref->getId(), 'dos' => $dos]);
                if ($cat1) {
                    $cat2 = $tree_repo->findOneBy(['erp_id'=>$cat_ref->getParent()]);
                    if ($cat2) {
                        $cat2 = $i18n_repo->findOneBy(['tree_label' => $cat2->getId(), 'dos' => $dos]);
                    }
                    $cat2 = $cat2 ? $cat2->getValue() : null;
                }
                $cat1 = $cat1 ? $cat1->getValue() : null;
            }
            
            $tmp_arts[] = [
                'id' =>  $cat->getId(),
                'dos' => $cat->getDos(),
                'category01' => $cat2,
                'category02' => $cat1,
                'gamme' => $cat->getGamme(),
                'art' => $cat->getArt(),
                'article' =>  $cat->getArticle(),
            ];
        }

        return Responses::download($tmp_arts, 'offers', $dos, $format);
    }
     /**
     * @Route("/online", name="export_online")
     */
    public function online( \Twig_Environment $twig )
    { 
        
        $root_data = $this->getParameter('root_data');
        
        $database = $this->getParameter('dbase');
        $user = $this->getParameter('db_user');
        $pass = $this->getParameter('db_pass');
        $host = $this->getParameter('db_host');
        $backup_name = $root_data . 'online/online.sql';
        exec("mysqldump --user={$user} --password={$pass} --host={$host} {$database} pim_products pim_articles hex_articles_tree hex_fields_i18n --result-file={$backup_name} 2>&1", $output, $return_var);
        ob_start();
        require('../data/online/online.sql');
        $sql = ob_get_flush();
        return $this->render('export/online.sql.twig', [
            'sql' => $sql,
        ]);
       
    }
}
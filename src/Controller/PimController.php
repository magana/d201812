<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Hexis\Sync\Outcoming\Pim;
use App\Entity\HexArticles;
use App\Entity\HexArticlesTree;
use App\Entity\HexFamilyRates;
use App\Hexis\Tools\Responses;
use App\Hexis\Tools\LangOptions;
use App\Hexis\Tools\WPFuncs;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;
use App\Hexis\Tools\Locale;

/**
* @Route("/pim")
*/
class PimController extends AbstractController
{
    private $pim;
    public function __construct(Pim $pim)
    {
        $this->pim = $pim;
    }
    /**
     * @Route("/", name="pim_index")
     */
    public function index(WPFuncs $wp,Request $request, \Twig_Environment $twig)
    {
        ini_set('memory_limit', '-1');
        $em = $this->getDoctrine()->getManager();

        $repo_arts = $em->getRepository(HexArticles::class);
       
        
        $repo_tree = $em->getRepository(HexArticlesTree::class);
        $els_arbo = [];
        $pim_arbo_produit_ref = [];
        $dos = [100,120,200,300,400,500];
        foreach ($dos as $d) {
            $cats= $repo_tree->getTreeWithDos($d);
            $gammes = $repo_tree->getGammeAndParent($d);
            if ($nb = count($cats) + count($gammes)) {
                $els_arbo[] = [
                    'results' => $nb,
                    'dos'     => $d
                ];
            }
            if ($nb = count($repo_arts->getArtsWithTree($d))) {
                $pim_arbo_produit_ref[] = [
                    'results' => $nb,
                    'dos'     => $d
                ];
            }
        }
        
        

        $locale = new Locale($request);
        $cookie = $locale->getCookie();
        $html =  $twig->render('pim/index.html.twig', [
            'is_logegged_id' => $wp->isLoggedIn(),
            'art_dos' => LangOptions::options($em),
            'els_arbo' => $els_arbo,
            'pim_arbo_produit_ref' => $pim_arbo_produit_ref,
            'topheader' => file_get_contents("{$request->getScheme()}://{$request->getHttpHost()}/mu-site/header/". $request->getLocale()),
        ]);
        $response = new Response($html);
        $response->headers->setCookie($cookie);
        return $response;       
    }

    /**
    * @Route("/pre-ref/{dos}", name="pim_pre_ref")
    */
    public function csvPreRef($dos = 100)
    {
        ini_set('memory_limit', '-1');
        $data = $this->pim->getPreRef($dos);
        return Responses::download($data, 'pre-ref', $dos, 'csv');
    }
    /**
    * @Route("/elements-arbo/{dos}", name="pim_elements_arbo")
    */
    public function csvElementsArbo($dos = 100)
    {
        ini_set('memory_limit', '-1');
        $data = $this->pim->getArboElementsRef($dos);
        return Responses::download($data, 'elements-arbo', $dos, 'csv');
    }
    /**
    * @Route("/arbo-produit-ref/{dos}", name="pim_arbo_produit_ref")
    */
    public function csvArboProdRef($dos = 100)
    {
        ini_set('memory_limit', '-1');
        $data = $this->pim->getArboProduitRef($dos);
        return Responses::download($data, 'arbo-produit-ref', $dos, 'csv');
    }
}
<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Hexis\Tools\LangOptions;
use App\Hexis\Tools\WPFuncs;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
* @Route("/mu-site")
*/
class MultisiteController extends AbstractController
{
    /**
     * @Route("/", name="mu_index")
     */
    public function index(WPFuncs $wp)
    {
        $em = $this->getDoctrine()->getManager();
        return $this->render('multisite/index.html.twig', [
            'is_logegged_id' => $wp->isLoggedIn(),
            'art_dos' => LangOptions::options($em),
        ]);
    }
    /**
     * @Route("/header/{_locale}", name="mu_header", defaults={"_locale": "en"})
     */
    public function header(Request $request)
    {
        return $this->render('multisite/main-header.html.twig', [
            'data_url' => "{$request->getScheme()}://{$request->getHttpHost()}",
        ]);
    }
    /**
     * @Route("/network", name="mu_network")
     */
    public function network()
    {
        $json = $this->getParameter('hexis_network');
        return new JsonResponse($json);
    }
}
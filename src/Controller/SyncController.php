<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Hexis\Sync\Incomming\Syncronize;
use App\Entity\Article;
use Symfony\Component\HttpKernel\Profiler\Profiler;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\HexArticles;
use App\Hexis\Tools\LangOptions;
use App\Hexis\Tools\WPFuncs;
use Symfony\Component\HttpFoundation\Cookie;
use App\Hexis\Tools\Locale;
use App\Hexis\Sync\Incomming\Orchestra\OrcStatus;
use App\Hexis\Sync\Incomming\FakeData\PimData;

/**
 * @Route("/sync")
 */
class SyncController extends AbstractController
{
	private $syncronize;
	private $hexis_locales;
	public function __construct(Syncronize $syncronize, $hexis_locales)
	{
		$this->syncronize = $syncronize;
		$this->hexis_locales = $hexis_locales;
	}
	/**
	 * @Route("/",name="sync_index")
	 */
	public function index(Request $request, \Swift_Mailer $mailer, WPFuncs $wp, \Twig_Environment $twig)
	{

		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 300);
		$loading = <<<OEL
		<div id="loading">Please wait...<br><span>This may take several minutes</span></div>
		<style>
		#loading {
			top:40%;
			font-family: "Open Sans", sans-serif;
			width: 80%;
			display: block;
			position: relative;
			color: #212121;
			background-color: #eee;
			padding: 1rem;
			margin: 1.5rem auto;
			text-align: center;
			border-radius: 10px;
			overflow: hidden;
		}
		#loading span{
			color:#555;
			font-size:0.8em;
		}
		
		#loading:after {
			content: "";
			position: absolute;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			background-color: rgba(0, 0, 0, 0.3);
			-webkit-animation: loadBar 2s cubic-bezier(0.09, 0.89, 0.7, 0.71) infinite;
			animation: loadBar 2s cubic-bezier(0.09, 0.89, 0.7, 0.71) infinite;
		}
		@-webkit-keyframes loadBar {
			0% {
				left: -110%;
			}
		
			100% {
				left: 110%;
			}
		}
		
		@keyframes loadBar {
			0% {
				left: -110%;
			}
		
			100% {
				left: 110%;
			}
		}
		</style>
OEL;
		ob_implicit_flush(true);
		echo $loading;
		ob_end_flush();

		ob_start();
		$this->syncronize->updateData();
		echo "\n------------- Time:" . number_format(microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'], 3);
		$output = ob_get_clean();

		$message = (new \Swift_Message('[DATA] - Sync DB data'))
			->setFrom('data@hexis.fr')
			->setTo('webmaster@hexis.fr')
			->setBody(
				$this->renderView(
					'emails/sync_log.html.twig',
					array('message' => $output)
				),
				'text/html'
			);

		$mailer->send($message);

		$em = $this->getDoctrine()->getManager();
		$locale = new Locale($request);
		$cookie = $locale->getCookie();
		$html = $twig->render('sync/output.html.twig', [
			'is_logegged_id' => $wp->isLoggedIn(),
			'art_dos' => LangOptions::options($em),
			'message' => $output,
			'topheader' => file_get_contents("{$request->getScheme()}://{$request->getHttpHost()}/mu-site/header/" . $request->getLocale()),
		]);
		$response = new Response($html);
		$response->headers->setCookie($cookie);
		return $response;
	}
	/**
	 * @Route("/status", name="sync_status")
	 */
	public function status(Request $request, WPFuncs $wp, \Twig_Environment $twig)
	{
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 300);
		$em = $this->getDoctrine()->getManager();
		$em->getConnection()->getConfiguration()->setSQLLogger(null);
		ob_start();
		$status = new OrcStatus($em);
		$output = ob_get_clean();
		$locale = new Locale($request);
		$cookie = $locale->getCookie();
		$html = $twig->render('sync/output.html.twig', [
			'is_logegged_id' => $wp->isLoggedIn(),
			'art_dos' => LangOptions::options($em),
			'message' => $output,
			'topheader' => file_get_contents("{$request->getScheme()}://{$request->getHttpHost()}/mu-site/header/" . $request->getLocale()),
		]);
		$response = new Response($html);
		$response->headers->setCookie($cookie);
		return $response;
	}
	/**
	 * @Route("/fake-data", name="sync_fake")
	 */
	public function fake(Request $request, WPFuncs $wp, \Twig_Environment $twig)
	{
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 300);
		$em = $this->getDoctrine()->getManager();
		$em->getConnection()->getConfiguration()->setSQLLogger(null);
		ob_start();
		$status = new PimData($em, $this->hexis_locales);
		$output = ob_get_clean();
		$locale = new Locale($request);
		$cookie = $locale->getCookie();
		$html = $twig->render('sync/output.html.twig', [
			'is_logegged_id' => $wp->isLoggedIn(),
			'art_dos' => LangOptions::options($em),
			'message' => $output,
			'topheader' => file_get_contents("{$request->getScheme()}://{$request->getHttpHost()}/mu-site/header/" . $request->getLocale()),
		]);
		$response = new Response($html);
		$response->headers->setCookie($cookie);
		return $response;
	}
}
<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Hexis\Tools\Files\CacheZip;
use App\Entity\HexArticles;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\BinaryFileResponse;


/**
* @Route("/download")
*/
class DownloadController extends AbstractController
{
    /**
     * @Route("/{mode}/{dos}/{parent}/{gamme}", name="zip_download")
     */
    public function index($mode='images', $dos=100, $parent=null, $gamme=null)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(HexArticles::class);
        $arts = $repo->getArtByCriteria($dos, $parent, $gamme); 
        
        if($mode == 'images'){
            $imgs=[];
            foreach ($arts as $art) {
                $img = $art->getArt();
                $img_src = realpath('../') . "/htdocs/products/{$img}.jpg";        
                if (file_exists($img_src)) {               
                    $imgs[] = [
                        'src' => $img_src,
                        'name'=> $img
                    ];
                }
            }
            $file_name = "{$mode}_{$dos}";
            $file_name .= '_' . ($parent=='null' ? "0" : $parent);
            $file_name .= '_' . ($gamme=='null' ? "0" : $gamme);
            $file_name .= '.zip';
            $zip = CacheZip::getFile(realpath('../'), $file_name, $imgs, $time=48);
        }

        $response = new BinaryFileResponse($zip['src']);
        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $zip['name']
        );
        $response->headers->set('Content-Disposition', $disposition);
        return $response;        
    }
}
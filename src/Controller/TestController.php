<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Hexis\Sync\Incomming\Orchestra\OrcStatus;

class TestController extends AbstractController
{
    /**
     * @Route("/test", name="test")
     */
    public function index()
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 300);
        $em = $this->getDoctrine()->getManager();
        ob_start();
        $status = new OrcStatus($em);
        $message = ob_get_clean();
        return $this->render('test/index.html.twig', [
            'message' => $message,
        ]);
    }
}
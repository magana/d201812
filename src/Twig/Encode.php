<?php
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class Encode extends AbstractExtension
{
    public function getFilters()
    {
        return array(
            new TwigFilter('utf8_decode', array($this, 'utf8Decode')),
        );
    }

    /**
     * utf8 decode string
     */
    public function utf8Decode($string) {
        return utf8_decode($string);
    }
}
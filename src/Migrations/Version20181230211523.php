<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181230211523 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE pim_products (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, metas LONGTEXT NOT NULL COMMENT \'(DC2Type:object)\', dos INT NOT NULL, ref VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hex_family_rates (id INT AUTO_INCREMENT NOT NULL, ref VARCHAR(255) NOT NULL, erp_id VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hex_articles (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, family_rate_id INT DEFAULT NULL, article VARCHAR(255) NOT NULL, dos INT NOT NULL, ref VARCHAR(255) NOT NULL, status INT NOT NULL, art VARCHAR(255) NOT NULL, date_created DATETIME NOT NULL, date_updated DATETIME NOT NULL, gamme VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, dim_unitaire VARCHAR(255) NOT NULL, dim_0001 VARCHAR(255) NOT NULL, dim_0002 VARCHAR(255) NOT NULL, poid_net VARCHAR(255) NOT NULL, poid_brut VARCHAR(255) NOT NULL, ean VARCHAR(255) NOT NULL, des LONGTEXT NOT NULL, poid_unitaire VARCHAR(255) NOT NULL, INDEX IDX_E682432912469DE2 (category_id), INDEX IDX_E6824329D18D4B23 (family_rate_id), INDEX search_idx (ref, dos, article), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pim_articles (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, article VARCHAR(255) NOT NULL, dos INT NOT NULL, ref VARCHAR(255) NOT NULL, status INT NOT NULL, gamme VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, dim_unitaire VARCHAR(255) NOT NULL, dim_0001 INT NOT NULL, dim_0002 INT NOT NULL, poid_net VARCHAR(255) NOT NULL, poid_brut VARCHAR(255) NOT NULL, ean VARCHAR(255) NOT NULL, des LONGTEXT NOT NULL, poid_unitaire VARCHAR(255) NOT NULL, art VARCHAR(255) NOT NULL, INDEX IDX_270B004012469DE2 (category_id), INDEX search_idx (ref, dos, article), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hex_articles_tree (id INT AUTO_INCREMENT NOT NULL, place INT NOT NULL, parent VARCHAR(255) DEFAULT NULL, erp_id VARCHAR(255) DEFAULT NULL, INDEX search_idx (erp_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hex_fields_i18n (id INT AUTO_INCREMENT NOT NULL, art_label_id INT DEFAULT NULL, tree_label_id INT DEFAULT NULL, rate_label_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, value LONGTEXT NOT NULL, dos VARCHAR(255) NOT NULL, INDEX IDX_6E3F8B846ABE562F (art_label_id), INDEX IDX_6E3F8B845075C442 (tree_label_id), INDEX IDX_6E3F8B84E46E3F98 (rate_label_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE hex_articles ADD CONSTRAINT FK_E682432912469DE2 FOREIGN KEY (category_id) REFERENCES hex_articles_tree (id)');
        $this->addSql('ALTER TABLE hex_articles ADD CONSTRAINT FK_E6824329D18D4B23 FOREIGN KEY (family_rate_id) REFERENCES hex_family_rates (id)');
        $this->addSql('ALTER TABLE pim_articles ADD CONSTRAINT FK_270B004012469DE2 FOREIGN KEY (category_id) REFERENCES hex_articles_tree (id)');
        $this->addSql('ALTER TABLE hex_fields_i18n ADD CONSTRAINT FK_6E3F8B846ABE562F FOREIGN KEY (art_label_id) REFERENCES hex_articles (id)');
        $this->addSql('ALTER TABLE hex_fields_i18n ADD CONSTRAINT FK_6E3F8B845075C442 FOREIGN KEY (tree_label_id) REFERENCES hex_articles_tree (id)');
        $this->addSql('ALTER TABLE hex_fields_i18n ADD CONSTRAINT FK_6E3F8B84E46E3F98 FOREIGN KEY (rate_label_id) REFERENCES hex_family_rates (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE hex_articles DROP FOREIGN KEY FK_E6824329D18D4B23');
        $this->addSql('ALTER TABLE hex_fields_i18n DROP FOREIGN KEY FK_6E3F8B84E46E3F98');
        $this->addSql('ALTER TABLE hex_fields_i18n DROP FOREIGN KEY FK_6E3F8B846ABE562F');
        $this->addSql('ALTER TABLE hex_articles DROP FOREIGN KEY FK_E682432912469DE2');
        $this->addSql('ALTER TABLE pim_articles DROP FOREIGN KEY FK_270B004012469DE2');
        $this->addSql('ALTER TABLE hex_fields_i18n DROP FOREIGN KEY FK_6E3F8B845075C442');
        $this->addSql('DROP TABLE pim_products');
        $this->addSql('DROP TABLE hex_family_rates');
        $this->addSql('DROP TABLE hex_articles');
        $this->addSql('DROP TABLE pim_articles');
        $this->addSql('DROP TABLE hex_articles_tree');
        $this->addSql('DROP TABLE hex_fields_i18n');
    }
}

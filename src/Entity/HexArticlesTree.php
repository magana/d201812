<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HexArticlesTreeRepository")
 * @ORM\Table(indexes={@ORM\Index(name="search_idx", columns={"erp_id"})})
 */
class HexArticlesTree
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $place;

  
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HexFieldsI18n", mappedBy="tree_label",cascade={"persist"})
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $erp_id;

    public function __construct()
    {
        $this->label = new ArrayCollection();
    }

   

   

    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlace(): ?string
    {
        return $this->place;
    }

    public function setPlace(string $place): self
    {
        $this->place = $place;

        return $this;
    }


    public function getParent(): ?string
    {
        return $this->parent;
    }

    public function setParent(?string $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|HexFieldsI18n[]
     */
    public function getLabel(): Collection
    {
        return $this->label;
    }

    public function addLabel(HexFieldsI18n $label): self
    {
        if (!$this->label->contains($label)) {
            $this->label[] = $label;
            $label->setTreeLabel($this);
        }

        return $this;
    }

    public function removeLabel(HexFieldsI18n $label): self
    {
        if ($this->label->contains($label)) {
            $this->label->removeElement($label);
            // set the owning side to null (unless already changed)
            if ($label->getTreeLabel() === $this) {
                $label->setTreeLabel(null);
            }
        }

        return $this;
    }

    public function getErpId(): ?string
    {
        return $this->erp_id;
    }

    public function setErpId(?string $erp_id): self
    {
        $this->erp_id = $erp_id;

        return $this;
    }

}
<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HexFamilyRatesRepository")
 */
class HexFamilyRates
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ref;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HexFieldsI18n", mappedBy="rate_label",cascade={"persist"})
     */
    private $label;

    /**
     * @ORM\Column(type="string")
     */
    private $erp_id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\HexArticles", mappedBy="family_rate", cascade={"persist", "remove"})
     */
    private $art_rate;



    public function __construct()
    {
        $this->label = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRef(): ?string
    {
        return $this->ref;
    }

    public function setRef(string $ref): self
    {
        $this->ref = $ref;

        return $this;
    }

    /**
     * @return Collection|HexFieldsI18n[]
     */
    public function getLabel(): Collection
    {
        return $this->label;
    }

    public function addLabel(HexFieldsI18n $label): self
    {
        if (!$this->label->contains($label)) {
            $this->label[] = $label;
            $label->setTreeLabel($this);
        }

        return $this;
    }

    public function removeLabel(HexFieldsI18n $label): self
    {
        if ($this->label->contains($label)) {
            $this->label->removeElement($label);
            // set the owning side to null (unless already changed)
            if ($label->getTreeLabel() === $this) {
                $label->setTreeLabel(null);
            }
        }

        return $this;
    }

    public function getErpId(): ?string
    {
        return $this->erp_id;
    }

    public function setErpId(string $erp_id): self
    {
        $this->erp_id = $erp_id;

        return $this;
    }

    public function getArtRate(): ?HexArticles
    {
        return $this->art_rate;
    }

    public function setArtRate(?HexArticles $art_rate): self
    {
        $this->art_rate = $art_rate;

        // set (or unset) the owning side of the relation if necessary
        $newFamily_rate = $art_rate === null ? null : $this;
        if ($newFamily_rate !== $art_rate->getFamilyRate()) {
            $art_rate->setFamilyRate($newFamily_rate);
        }

        return $this;
    }
}
<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HexFieldsI18nRepository")
 */
class HexFieldsI18n
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $value;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $dos;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\HexArticles", inversedBy="label",cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $art_label;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\HexArticlesTree", inversedBy="label")
     * @ORM\JoinColumn(nullable=true)
     */
    private $tree_label;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\HexFamilyRates", inversedBy="label")
     */
    private $rate_label;

    

    
   
    
  
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getDos(): ?string
    {
        return $this->dos;
    }

    public function setDos(string $dos): self
    {
        $this->dos = $dos;

        return $this;
    }

    public function getArtLabel(): ?HexArticles
    {
        return $this->art_label;
    }

    public function setArtLabel(?HexArticles $art_label): self
    {
        $this->art_label = $art_label;

        return $this;
    }

    public function getTreeLabel(): ?HexArticlesTree
    {
        return $this->tree_label;
    }

    public function setTreeLabel(?HexArticlesTree $tree_label): self
    {
        $this->tree_label = $tree_label;

        return $this;
    }

    public function getRateLabel(): ?HexFamilyRates
    {
        return $this->rate_label;
    }

    public function setRateLabel(?HexFamilyRates $rate_label): self
    {
        $this->rate_label = $rate_label;

        return $this;
    }
    
}
<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HexArticlesRepository")
 * @ORM\Table(indexes={@ORM\Index(name="search_idx", columns={"ref", "dos","article"})})
 */
class HexArticles
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $article;


    /**
     * @ORM\Column(type="integer")
     */
    private $dos;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ref;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $art;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_updated;

   
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $gamme;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $dim_unitaire;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $dim_0001;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $dim_0002;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $poid_net;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $poid_brut;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ean;

       /**
     * @ORM\ManyToOne(targetEntity="App\Entity\HexArticlesTree", inversedBy="car_erp")
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HexFieldsI18n", mappedBy="art_label", orphanRemoval=true)
     */
    private $label;

    /**
     * @ORM\Column(type="text")
     */
    private $des;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\HexFamilyRates", inversedBy="art_rate")
    */
    private $family_rate;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $poid_unitaire;

  
 
  
    public function __construct()
    {
        $this->label = new ArrayCollection();
    }


    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArticle(): ?string
    {
        return $this->article;
    }

    public function setArticle(string $article): self
    {
        $this->article = $article;

        return $this;
    }

    

    public function getDos(): ?int
    {
        return $this->dos;
    }

    public function setDos(int $dos): self
    {
        $this->dos = $dos;

        return $this;
    }

    public function getRef(): ?string
    {
        return $this->ref;
    }

    public function setRef(string $ref): self
    {
        $this->ref = $ref;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getArt(): ?string
    {
        return $this->art;
    }

    public function setArt(string $art): self
    {
        $this->art = $art;

        return $this;
    }

    public function getDateCreated(): ?\DateTimeInterface
    {
        return $this->date_created;
    }

    public function setDateCreated(\DateTimeInterface $date_created): self
    {
        $this->date_created = $date_created;

        return $this;
    }

    public function getDateUpdated(): ?\DateTimeInterface
    {
        return $this->date_updated;
    }

    public function setDateUpdated(\DateTimeInterface $date_updated): self
    {
        $this->date_updated = $date_updated;

        return $this;
    }
    public function getGamme(): ?string
    {
        return $this->gamme;
    }

    public function setGamme(string $gamme): self
    {
        $this->gamme = $gamme;

        return $this;
    }

 
    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDimUnitaire(): ?string
    {
        return $this->dim_unitaire;
    }

    public function setDimUnitaire(string $dim_unitaire): self
    {
        $this->dim_unitaire = $dim_unitaire;

        return $this;
    }

    public function getDim0001(): ?string
    {
        return $this->dim_0001;
    }

    public function setDim0001(string $dim_0001): self
    {
        $this->dim_0001 = $dim_0001;

        return $this;
    }

    public function getDim0002(): ?string
    {
        return $this->dim_0002;
    }

    public function setDim0002(string $dim_0002): self
    {
        $this->dim_0002 = $dim_0002;

        return $this;
    }

    public function getPoidNet(): ?string
    {
        return $this->poid_net;
    }

    public function setPoidNet(string $poid_net): self
    {
        $this->poid_net = $poid_net;

        return $this;
    }

    public function getPoidBrut(): ?string
    {
        return $this->poid_brut;
    }

    public function setPoidBrut(string $poid_brut): self
    {
        $this->poid_brut = $poid_brut;

        return $this;
    }

    public function getEan(): ?string
    {
        return $this->ean;
    }

    public function setEan(string $ean): self
    {
        $this->ean = $ean;

        return $this;
    }

    public function getCategory(): ?HexArticlesTree
    {
        return $this->category;
    }

    public function setCategory(?HexArticlesTree $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection|HexFieldsI18n[]
     */
    public function getLabel(): Collection
    {
        return $this->label;
    }

    public function addLabel(HexFieldsI18n $label): self
    {
        if (!$this->label->contains($label)) {
            $this->label[] = $label;
            $label->setArtLabel($this);
        }

        return $this;
    }

    public function removeLabel(HexFieldsI18n $label): self
    {
        if ($this->label->contains($label)) {
            $this->label->removeElement($label);
            // set the owning side to null (unless already changed)
            if ($label->getArtLabel() === $this) {
                $label->setArtLabel(null);
            }
        }

        return $this;
    }

    public function getDes(): ?string
    {
        return $this->des;
    }

    public function setDes(string $des): self
    {
        $this->des = $des;

        return $this;
    }

    public function getFamilyRate(): ?HexFamilyRates
    {
        return $this->family_rate;
    }

    public function setFamilyRate(?HexFamilyRates $family_rate): self
    {
        $this->family_rate = $family_rate;

        return $this;
    }

    public function getPoidUnitaire(): ?string
    {
        return $this->poid_unitaire;
    }

    public function setPoidUnitaire(string $poid_unitaire): self
    {
        $this->poid_unitaire = $poid_unitaire;

        return $this;
    }

}
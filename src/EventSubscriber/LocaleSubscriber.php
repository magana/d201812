<?php 
namespace App\EventSubscriber;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class LocaleSubscriber implements EventSubscriberInterface
{
    private $defaultLocale;
    private $locales;

    public function __construct($defaultLocale = 'en', Array $hexis_locales)
    {
        $this->defaultLocale = $defaultLocale;
        $this->locales = $hexis_locales;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $lng= $request->cookies->get('hex_lang') ?: 100;
        $cookie = $this->locales[$lng];
        $request->setLocale($cookie);
    }

    public static function getSubscribedEvents()
    {
        return [
            // must be registered before (i.e. with a higher priority than) the default Locale listener
            KernelEvents::REQUEST => [['onKernelRequest', 25]],
        ];
    } 
}
<?php
namespace App\Hexis\Tools\Files;

use App\Hexis\Tools\Files\File;
use ZipArchive;


class CacheZip 
{
	static private $cache_folder = "/htdocs/docs/zip/";

	static public function getFile($real_path, $file_name, $array_images, $time=24)
	{

		$file_life = $time * 3600;
		$file = $real_path . CacheZip::$cache_folder .$file_name;

		if(file_exists($file) ){
			if((time() - filemtime($file)) >$file_life){
				CacheZip::write($real_path, $file_name, $array_images);
			}
		}else{
			CacheZip::write($real_path, $file_name, $array_images);
		}		
		return [
			'name'=> $file_name,
			'src' => $file
		];
	}
	static private function write($real_path, $file_name, $array_images)
	{
		$target = $real_path . CacheZip::$cache_folder .$file_name;
		
		$file = new File($target);
		$file->write("");

		$zip = new ZipArchive;
		$zip->open($target, ZipArchive::CREATE);
		foreach ($array_images as $img) {	
			$zip->addFile($img['src'],$img['name']);
		}
	
		$zip->close();	
	}
}
<?php
namespace App\Hexis\Tools\Files;


class Directory
{

	private $path = '';
	private $msg = '';

	function __construct( $path=null )
	{
		$this->path = $path;
	}

	public function create( $path=null )
	{
		if ( !$path ) {
			return false;
		}
		if( !$this->exists( $path ) ) {
			@mkdir( $path, 0777, true);
		}
	}
	function recursiveCopy( $source, $destination )
	{
		if ( !$this->exists( $destination ) ) {
			mkdir( $destination, 0777, true );
		}
		$splFileInfoArr = new \RecursiveIteratorIterator( new \RecursiveDirectoryIterator( $source), \RecursiveIteratorIterator::SELF_FIRST );
		foreach ( $splFileInfoArr as $fullPath => $splFileinfo) {
			if (in_array( $splFileinfo->getBasename(), [".", ".."] ) ) {
				continue;
			}
			$path = str_replace( $source, "", $splFileinfo->getPathname() );

			if ( $splFileinfo->isDir() ) {
				if( !file_exists( $destination . "/" . $path ) ) mkdir( $destination . "/" . $path, 0777, true );
			} else {
				copy( $fullPath, $destination . "/" . $path );
			}
		}
		$this->msg = sprintf( 'Directory copied: %s ', $destination );
	}
	public function getMsg()
	{
		return $this->msg;
	}

	public function exists( $path=null)
	{
		if ( $exist = @file_exists( $path) ){
			$this->msg = sprintf( 'Directory exists: %s', $path );
		}else{
			$this->msg = sprintf( 'Directory created: %s', $path );
		}
		return $exist;
	}

}

?>
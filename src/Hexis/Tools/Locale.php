<?php
namespace App\Hexis\Tools;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;


class Locale
{
    private $cookie;
    public function __construct(Request $request)
    {
        $lang = $request->cookies->get('hex_lang') ?: "100";
        $time = time() + 3600 * 24 * 30;
        $cookie = new Cookie('hex_lang', $lang, $time, "/", null, false, false);        
        $this->cookie =  $cookie;
        $request->setLocale($this->getLocale($lang));
    }
    public function getCookie()
    {
        return $this->cookie;
    }
    public function getLocale($dos) {
        $codes = [
          0 => 'en',
          100 => 'fr',
          200 => 'de',
          300 => 'it',
          400 => 'es',
          500 => 'en',
        ];
        return $codes[$dos];
      }

}
<?php
namespace App\Hexis\Tools;

class Message
{
	static function write( string $msg, $level = 1 )
	{
        $levels=[
            '-',
            '  |_',
            '    |_',
            '      |_',
            '        |_',
            '          |_',
        ];
        echo $levels[$level] . $msg . PHP_EOL;
 	}
}
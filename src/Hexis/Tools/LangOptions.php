<?php
namespace App\Hexis\Tools;

use Doctrine\ORM\EntityManager;
use App\Entity\HexArticles;



class LangOptions
{
    static public function options(EntityManager $em){
        $repo_arts = $em->getRepository(HexArticles::class);
        $art_dos = $repo_arts->getNbArtsByDos();
        $langs = [
            '100' => 'French',
            '200' => 'German',
            '300' => 'Italian',
            '400' => 'Spanish',
            '500' => 'Sweden'
        ];
        foreach ($art_dos as $key => $dos) {
            if ($dos['dos']!=120 && $dos['dos']!= 'all') {
                $art_dos[$key]['lang'] = $langs[$dos['dos']];
            } else {
                unset($art_dos[$key]);
            }
        }
        return $art_dos;
    }
}
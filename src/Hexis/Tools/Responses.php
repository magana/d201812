<?php
namespace App\Hexis\Tools;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class Responses
{
	static public function download($data, $filename, $dos, $format)
    {
        $encoders = [new XmlEncoder(), new JsonEncoder(), new CsvEncoder()];
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $normalizers = [new DateTimeNormalizer(), $normalizer];
        $serializer = new Serializer($normalizers, $encoders);

        $data = $serializer->serialize($data, $format);
        $data = iconv( mb_detect_encoding( $data ), 'Windows-1252//TRANSLIT', $data );
        $filename = "{$filename}-{$dos}.{$format}";
        $response = new Response($data);
        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );
        $response->headers->set('Content-Disposition', $disposition);
        return $response;
    }
}
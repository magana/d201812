<?php
namespace App\Hexis\Sync\Outcoming;


use Doctrine\ORM\EntityManagerInterface;
use App\Hexis\Tools\Message;
use App\Entity\HexArticles;
use App\Entity\HexArticlesTree;
use App\Entity\HexFieldsI18n;
use App\Entity\HexFamilyRates;





class Pim
{
    private $em;
    public function __construct(EntityManagerInterface $em)  
    {
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);          

    }
    public function getPreRef($dos)
    {
        $repo_arts = $this->em->getRepository(HexArticles::class);
        $cretaria = ['dos' => $dos];
        $arts = $repo_arts->findBy($cretaria);
        $tmp_arts = [];
        foreach ($arts as $art) {
            $tmp_arts[] = [
                'idEntite' =>  '',
                'ID_FOUR' =>  'HEXIS',
                'CODE_ARTICLE' =>   $art->getArticle(),
                'DESIGNATION_ART_1' =>  $art->getDes(),///1
                'STATUT_PREREF' =>  $art->getStatus(),
                'STATUT' =>  $art->getStatus()==1? 'Actif':'Inactif',
                'NCARTI' =>  $art->getRef(),
                'NADESI' =>  $art->getDes(),///1
                'NADSC1' =>  '',
                'NADSC2' =>  '',
                'NATYPE' =>  $art->getType(),
                'NCSSAR' =>  $art->getArt(),
                'NSADES' =>  '',
                'NSACO1' =>  '',
            ];           
        }
        return $tmp_arts;
    }
    public function getArboProduitRef($dos)
    {
        $repo_arts = $this->em->getRepository(HexArticles::class);
        $arts = $repo_arts->getArtsWithTree($dos);
        $tmp_array = [];
        foreach ($arts as $art) {
            $gamme = "G_{$art['Gamme']}_{$art['erp_id']}";
            $tmp_array[] = [
                'TreeName1' => $art['TreeName1'],
                'TreeName2' => $art['TreeName2'],
                'TreeName3' => $gamme,
                'CodeProduit' => $art['art'],
                'LibelleProduit' => $this->compareLabelsResult($dos, $art['art']),
                'RefCat' => $art['article'],
                'LibelleArt' => $art['des'],
            ];
        }
        return $tmp_array;

    }
    public function compareLabelsResult($dos, $art){
        $repo_arts = $this->em->getRepository(HexArticles::class);
        $criteria = ['dos' => $dos, 'art' => $art];
        $labels = $repo_arts->findBy($criteria);
        $last = $this->normalizeForComparation($labels[0]->getDes());
        foreach ($labels as $label) {
            $current = $this->normalizeForComparation($label->getDes());
            if($last != $current){
                return "";
            }
        }       
        return $label->getDes();
    }
    private function normalizeForComparation($label){
        
        
        return  preg_replace(['/[^a-z0-9\s]/i', '/(\d)\s/', '/\s(\d)/','/\s+/'],".",strtolower( str_replace([". "], ".", $label)));
    }
    public function getArboElementsRef($dos)
    {
        $repo_tree = $this->em->getRepository(HexArticlesTree::class);
        $cats= $repo_tree->getTreeWithDos($dos);
        $gammes = $repo_tree->getGammeAndParent($dos);
        $tmp_array = [];
        foreach ($cats as $cat) {
            $labels = $cat->getLabel();
            foreach ($labels as $label) {
                if ($label->getDos() == $dos) {
                    break;
                }
            }
            $label = $label->getValue();
            $parent = $cat->getParent();
            $tmp_array[] = [
                'NomArbo'       => $label,
                'LibelleArbo'   => '',
                'IdArboDef'     => 2,
                'IdNiveauArbo'  => !$parent ? 2 : 3,
                'IdArboParent'  => '',
                'IdArboSource'  => 0,
                'NiveauArbo'    => !$parent ? 1 : 2,
                'codeArboParent'=> $parent,
                'codeArbo'      => $cat->getErpId(),
                'Position'      => $cat->getPlace(),
                'CodeCarac1'    => !$parent ? 'ID_CAT_NIV_1' : 'ID_CAT_NIV_2',
                'ValeurCarac1'  => $cat->getErpId(),
                'CodeCarac2'    => !$parent ? 'LABEL_CAT_NIV_1' : 'LABEL_CAT_NIV_2',
                'ValeurCarac2'  => $label,
                'CodeCarac3'    => '',
                'ValeurCarac3'  => '',
            ];
        }
        foreach ($gammes as $gamme) {

            $label = "G_{$gamme['gamme']}_{$gamme['erp_id']}";
            $parent = $gamme['erp_id'];
            $tmp_array[] = [
                'NomArbo'       => $label,
                'LibelleArbo'   => '',
                'IdArboDef'     => 2,
                'IdNiveauArbo'  => 4,
                'IdArboParent'  => '',
                'IdArboSource'  => 0,
                'NiveauArbo'    => 3,
                'codeArboParent'=> $parent,
                'codeArbo'      => "{$parent}-{$gamme['gamme']}",
                'Position'      => 1,
                'CodeCarac1'    => 'ID_GAMME',
                'ValeurCarac1'  => "{$parent}-{$gamme['gamme']}",
                'CodeCarac2'    => 'LABEL_GAMME',
                'ValeurCarac2'  => $label,
                'CodeCarac3'    => 'ARGUMENTAIRE_GAMME',
                'ValeurCarac3'  => '',
            ];
        }
        return $tmp_array;
    }
 
}
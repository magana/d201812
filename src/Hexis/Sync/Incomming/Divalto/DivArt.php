<?php
namespace App\Hexis\Sync\Incomming\Divalto;

use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Doctrine\ORM\EntityManager;
use App\Entity\HexArticles;
use App\Hexis\Tools\Message;
use App\Entity\HexFieldsI18n;
use App\Entity\HexArticlesTree;
use App\Entity\HexFamilyRates;
use DateTime;


class DivArt
{
    private $em;
    private $root_data;
    private $file = 'Diva_art.csv';
    
    public function __construct(EntityManager $em, String $root_data)
    {
        $this->em = $em;
        $this->root_data = $root_data;
    }
    public function updateData()
    {
        Message::write("Retriving {$this->file}", 3);
        $this->getRates();
        $this->getArts();
    }
    private function getRates()
    {
        Message::write("Build Rates table", 4);
        $file = $this->root_data . $this->file;

        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder(';')]);
        $counter = 0;
        $batch_size = 30;
        if (($handle = fopen($file, 'r')) !== false) {
            $header = fgets($handle);
            $repo_rates = $this->em->getRepository(HexFamilyRates::class);
            while (($line = fgets($handle)) !== false) {
                $line = $serializer->decode($header . $line, 'csv');
                
                $ref = isset($line['REF']) ? trim($line['REF']) : "";
                if ((isset($line['FAM_0002']) && substr( $line['FAM_0002'], 0 ) !== "-" && strpos($line['FAM_0002'], 'TARIF')===0) || strlen($ref)==4) {
                    
                    $dos = (int)$line['DOS'];
                                        
                    $rate = $repo_rates->findOneBy(['ref' => $ref]);
                    
                    if (!$rate) {
                        $counter ++;
                        $rate = new HexFamilyRates();
                        $rate->setErpId(trim(str_replace(' ', '', $line['ALZCATCOD'].$line['ALZSSCATCOD'])))
                        ->setRef($ref);                         
                    }
                    if ($rate) {
                        $label = utf8_decode(trim($line['DES']));
                        $des = new HexFieldsI18n();
                        $des->setName('DESIGNATION')
                        ->setDos($dos)
                        ->setValue($label)
                        ->setRateLabel($rate)
                        ->setTreeLabel(null)
                        ->setArtLabel(null);
                                
                        $this->em->persist($rate);
                        $this->em->persist($des);
                        $this->em->flush();
                        $this->em->clear();
                    }
                    
                    if (($counter % 500) == 0) {
                        //Message::write("$counter lines added", 5);
                    }
                }
            }
            
            Message::write("$counter lines added", 5);
            Message::write("Done", 4);
            fclose($handle);
        }
    }
    private function getArts()
    {
        Message::write("Build Arts table", 4);
        $file = $this->root_data . $this->file;

        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder(';')]);
        $counter = 0;
        $batch_size = 30;
        if (($handle = fopen($file, 'r')) !== false) {
            $header = fgets($handle);
            $repo_tree = $this->em->getRepository(HexArticlesTree::class);
            $repo_rates = $this->em->getRepository(HexFamilyRates::class);
            $repo_i18n = $this->em->getRepository(HexFieldsI18n::class);
            while (($line = fgets($handle)) !== false) {
                $line = str_replace('  ', '', $line);
                $line = $serializer->decode($header . $line, 'csv');
                
                $ref = isset($line['REF']) ? trim($line['REF']) : false;
                if ((isset($line['DOS']) && substr($line['DOS'], 0, 1) !== "-" && strpos($line['FAM_0002'], 'TARIF')===false) &&  strlen($ref)>4 ) {
                    $counter ++;
                    $dos = (int)$line['DOS'];
                    $taref = trim(str_replace(' ', '', $line['TAREF']));
                    $rate = $repo_rates->findOneBy(['ref' => $taref]);
                    $game="";
                    if ($rate) {
                        $rate_des = $repo_i18n->findOneBy([
                            'rate_label' => $rate->getId(),
                            //'dos' => $dos // Uncomment when all dos are set on rates
                        ]);
                        if ($rate_des) {
                            $rate_ref = $rate->getRef();
                            preg_match('/\s+G(?<game>[0-9]+)\s+/', $rate_des->getValue(), $matches);
                            $game = ($matches) ?  $game=$matches['game']: 0 ;
                        }
                    }
                    
                    $date_created = $line['USERCRDH'];
                    $arr = explode(".", $date_created);
                    $date_created = DateTime::createFromFormat('Y-m-d H:i:s', reset($arr));
                    $date_created = $date_created ?: new DateTime("now");

                    $date_updated = $line['USERMODH'];
                    $arr = explode(".", $date_updated);
                    $date_updated = DateTime::createFromFormat('Y-m-d H:i:s', reset($arr));
                    $date_updated = $date_updated ?: new DateTime("now");

                    $label = (trim($line['DES']));
                    $art = new HexArticles();
                    $art->setArticle($ref)
                    ->setDos($dos)
                    ->setRef(trim($line['INF_REF_EQUIV']))
                    ->setArt(trim($line['INF_ART_EQUIV']))
                    ->setStatus(0)
                    ->setDateCreated($date_created)
                    ->setDateUpdated($date_updated)                       
                    ->setGamme($game)
                    ->setType($line['REFUN'])
                    ->setDim0001($line['DIM_0001'])
                    ->setDim0002($line['DIM_0002'])
                    ->setPoidUnitaire($line['POIUN'])
                    ->setPoidBrut($line['POIB'])
                    ->setPoidNet($line['POIN'])
                    ->setEan($line['EAN'])
                    ->setDimUnitaire($line['DIMUN'])
                    ->setDes($label)
                    ->setFamilyRate($rate);                    
                    if($rate){
                        $tree_el = $repo_tree->findOneBy(['erp_id' => $rate->getErpId()]);
                        $art->setCategory($tree_el);
                    } else {
                        $art->setCategory(null);
                    }                                
                    $this->em->persist($art);
   
                    

                    if (($counter % $batch_size) === 0) {
                        $this->em->flush();
                        $this->em->clear();
                    }
                    if (($counter % 1000) == 0) {
                        //Message::write("$counter lines added", 5);
                    }
                }
            }
            
            Message::write("$counter lines added", 5);
            Message::write("Done", 4);
            $this->em->flush();
            $this->em->clear();
            fclose($handle);
        }
    }
}
<?php
namespace App\Hexis\Sync\Incomming\Divalto;

use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Doctrine\ORM\EntityManager;
use App\Hexis\Tools\Message;
use App\Entity\HexFieldsI18n;
use App\Entity\HexArticlesTree;

class DivT2XX
{
    private $em;
    private $root_data;

    public function __construct(EntityManager $em, String $root_data)
    {
        $this->em = $em;
        $this->root_data = $root_data;
    }
    public function updateData()
    {
        $this->getCsv(50);
        $this->getCsv(49);
    }
    private function getCsv(int $file_id)
    {

        $file = "Diva_T2{$file_id}.csv";
        Message::write("Retriving $file", 3);
        $file = $this->root_data . $file;

        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder(';')]);
        $counter = 0;
        $batch_size = 30;
        if (($handle = fopen($file, 'r')) !== false) {
            $header = fgets($handle);
            $repo_tree = $this->em->getRepository(HexArticlesTree::class);
           while (($line = fgets($handle)) !== false) {
                $line = str_replace('  ', '', $line);
                $line = $serializer->decode($header . $line, 'csv');
                if(isset($line['ALZNUMORDRE']) && substr( $line['ALZNUMORDRE'], 0,2) !== "--"){
                    $counter ++;
                    
                    $cat = new HexArticlesTree();
                    if($file_id==50){
                        $erpid = str_pad($line['ALZCATCOD'], 4, '0', STR_PAD_LEFT);
                        //$erpid = str_pad($erpid, 8, '0', STR_PAD_RIGHT);
                        $cat = $repo_tree->findOneBy(['erp_id' => $erpid]);
                        if(!$cat){
                            $cat = new HexArticlesTree();
                            $cat->setParent(null)
                            ->setErpId($erpid) 
                            ->setPlace($line['ALZNUMORDRE']?:0);
                        }
                        
                    }else{
                        $erpid = str_replace(' ', '', $line['ALZCATCOD'].$line['ALZSSCATCOD']);
                        $cat = $repo_tree->findOneBy(['erp_id' => $erpid]);
                        if(!$cat){
                            $cat = new HexArticlesTree();
                            $cat->setParent(null)
                            ->setPlace($line['ALZNUMORDRE']?:0)
                            ->setParent($line['ALZCATCOD'])
                            ->setErpId( $erpid );
                        }
                        
                    }
                       
					
                    $label = new HexFieldsI18n();
                    $label->setName('label')
                    ->setDos($line['DOS'])
					->setValue(utf8_encode($line['LIB80']))
					->setTreeLabel($cat)
                    ->setArtLabel(null)
                    ->setRateLabel(null);

                    $this->em->persist($label); 
                    $this->em->persist($cat); 
                    $this->em->flush();
                    $this->em->clear();                     
                    if(($counter % 100) == 0){                   
                        //Message::write("$counter lines added", 4);
                    }
                }
            }
            Message::write("$counter lines added", 4);
            Message::write("Done", 3);
            fclose($handle);           
        }
    }   
}
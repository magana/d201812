<?php
namespace App\Hexis\Sync\Incomming\OneBase;

use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Doctrine\ORM\EntityManager;
use App\Entity\HexArticles;
use App\Hexis\Tools\Message;
use App\Entity\HexFieldsI18n;
use App\Entity\HexArticlesTree;
use App\Entity\HexFamilyRates;
use DateTime;

class Products
{
    private $em;
    private $root_data;
    
    public function __construct(EntityManager $em, String $root_data)
    {
        $this->em = $em;
        $this->root_data = $root_data;
        Message::write("Init OneBase Sync", 2);
    }
    public function updateData()
    {
        $langs = [
            '100' => 'French',
            '200' => 'German',
            '300' => 'Italian',
            '400' => 'Spanish',
            '500' => 'Sweden'
        ];
        foreach ($langs as $key => $lang) {
            $this->getProducts($key, $lang);
        }
        
    }
    private function getProducts($dos, $lang)
    {
        Message::write("Retriving {$lang}", 3);
        $file = $this->root_data . $file;

        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder(';')]);
        $counter = 0;
        $batch_size = 30;
        if (($handle = fopen($file, 'r')) !== false) {
            $header = fgets($handle);
            $repo_tree = $this->em->getRepository(HexArticlesTree::class);
            $repo_rates = $this->em->getRepository(HexFamilyRates::class);
            $repo_i18n = $this->em->getRepository(HexFieldsI18n::class);
            while (($line = fgets($handle)) !== false) {
                $line = str_replace('  ', '', $line);
                $line = $serializer->decode($header . $line, 'csv');
                
                $ref = isset($line['REF']) ? trim($line['REF']) : false;
                if ((isset($line['DOS']) && substr($line['DOS'], 0, 1) !== "-" && strpos($line['FAM_0002'], 'TARIF')===false) &&  strlen($ref)>4 ) {
                    $counter ++;
                    $dos = (int)$line['DOS'];
                    $taref = trim(str_replace(' ', '', $line['TAREF']));
                    $rate = $repo_rates->findOneBy(['ref' => $taref]);
                    $game="";
                    if ($rate) {
                        $rate_des = $repo_i18n->findOneBy([
                            'rate_label' => $rate->getId(),
                            //'dos' => $dos // Uncomment when all dos are set on rates
                        ]);
                        if ($rate_des) {
                            $rate_ref = $rate->getRef();
                            preg_match('/\s+G(?<game>[0-9]+)\s+/', $rate_des->getValue(), $matches);
                            $game = ($matches) ?  $game=$matches['game']: 0 ;
                        }
                    }
                    
                    $date_created = $line['USERCRDH'];
                    $arr = explode(".", $date_created);
                    $date_created = DateTime::createFromFormat('Y-m-d H:i:s', reset($arr));
                    $date_created = $date_created ?: new DateTime("now");

                    $date_updated = $line['USERMODH'];
                    $arr = explode(".", $date_updated);
                    $date_updated = DateTime::createFromFormat('Y-m-d H:i:s', reset($arr));
                    $date_updated = $date_updated ?: new DateTime("now");

                    $label = (trim($line['DES']));
                    $art = new HexArticles();
                    $art->setArticle($ref)
                    ->setDos($dos)
                    ->setRef(trim($line['INF_REF_EQUIV']))
                    ->setArt(trim($line['INF_ART_EQUIV']))
                    ->setStatus(0)
                    ->setDateCreated($date_created)
                    ->setDateUpdated($date_updated)                       
                    ->setGamme($game)
                    ->setType($line['REFUN'])
                    ->setDim0001($line['DIM_0001'])
                    ->setDim0002($line['DIM_0002'])
                    ->setPoidUnitaire($line['POIUN'])
                    ->setPoidBrut($line['POIB'])
                    ->setPoidNet($line['POIN'])
                    ->setEan($line['EAN'])
                    ->setDimUnitaire($line['DIMUN'])
                    ->setDes($label)
                    ->setFamilyRate($rate);                    
                    if($rate){
                        $tree_el = $repo_tree->findOneBy(['erp_id' => $rate->getErpId()]);
                        $art->setCategory($tree_el);
                    } else {
                        $art->setCategory(null);
                    }                                
                    $this->em->persist($art);
   
                    

                    if (($counter % $batch_size) === 0) {
                        $this->em->flush();
                        $this->em->clear();
                    }
                    if (($counter % 1000) == 0) {
                        //Message::write("$counter lines added", 5);
                    }
                }
            }
            
            Message::write("$counter lines added", 5);
            Message::write("Done", 4);
            $this->em->flush();
            $this->em->clear();
            fclose($handle);
        }
    }
}
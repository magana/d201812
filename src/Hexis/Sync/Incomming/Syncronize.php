<?php
namespace App\Hexis\Sync\Incomming;

use Psr\Log\LoggerInterface;
use App\Hexis\Sync\Incomming\Divalto\DivArt;
use App\Hexis\Sync\Incomming\Divalto\DivT2XX;
use App\Hexis\Sync\Incomming\Orchestra\OrcFiles;
use Doctrine\ORM\EntityManagerInterface;
use App\Hexis\Tools\Message;
use App\Hexis\Sync\Incomming\Orchestra\OrcStatus;





class Syncronize
{
    private $em;
    private $root_data;
    public function __construct(EntityManagerInterface $em, String $root_data)  
    {
        $this->em = $em;
        $this->root_data = $root_data. "/data/erp/"; 

        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);      
    }
    
    public function updateData()
    {
        
        $this->cleanTables();
        Message::write('Sync started');        
        $this->updateDivaltoData();
        $this->updateOrchestra();
        $this->fixStatusPerOffers();
    }
    private function updateOrchestra()
    {
        Message::write('Orchestra sync started',2);
        $orc_files = new OrcFiles($this->em, $this->root_data );
        $orc_files->updateData();
    }
    private function updateDivaltoData()
    {
        Message::write('Divalto sync started', 2);
        
        $div_tree = new DivT2XX($this->em, $this->root_data );
        $div_tree->updateData();

        $div_art = new DivArt($this->em, $this->root_data );
        $div_art->updateData();
    }
    private function fixStatusPerOffers()
    {
        Message::write('Sync status per offer', 2);
        $orc_status = new OrcStatus($this->em);
    }

    protected function cleanTables()
    {
        Message::write('Truncate tables');
        $class_names = ['App\Entity\HexArticles', 'App\Entity\HexArticlesTree','App\Entity\HexFieldsI18n','App\Entity\HexFamilyRates' ];
        foreach ($class_names as $class_name) {
            $this->truncateTable($class_name);
        }
    }
    protected function truncateTable($class_name)
    {
        $class_metadata = $this->em->getClassMetadata($class_name);
        $connection = $this->em->getConnection();
        $db_Platform = $connection->getDatabasePlatform();
        $connection->beginTransaction();
        $connection->query('SET FOREIGN_KEY_CHECKS=0');
        $q = $db_Platform->getTruncateTableSql($class_metadata->getTableName());
        $connection->executeUpdate($q);
        $connection->query('SET FOREIGN_KEY_CHECKS=1');
        $connection->commit();
        Message::write("$class_name truncated", 2);
        
    }   
}
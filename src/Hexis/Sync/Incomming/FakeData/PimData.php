<?php
namespace App\Hexis\Sync\Incomming\FakeData;

use Doctrine\ORM\EntityManager;
use App\Hexis\Tools\Message;
use App\Entity\PimProducts;
use App\Entity\HexArticles;
use App\Entity\HexArticlesTree;
use App\Entity\HexFamilyRates;
use App\Entity\HexFieldsI18n;
use App\Hexis\Tools\WPFuncs;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Hexis\Tools\Locale;

class PimData
{
    private $em;
    private $wp_json = 'http://cdn.hexis-graphics.com/export/wp-json/';
    private $locales;

    public function __construct(EntityManager $em, $hexis_locales)
    {
        $this->em = $em;
        $this->locales = $hexis_locales;
        $this->updateData();
    }
    public function updateData()
    {
        Message::write("Getting Fake Data", 0);
        $this->cleanTables();
        $this->duplicateArts();
        $this->useWPData();
    }
    private function cleanTables()
    {
        Message::write("Truncate tables", 1);
        $connection = $this->em->getConnection();
        $statement = $connection->prepare("TRUNCATE TABLE pim_articles; TRUNCATE TABLE pim_products;");
        $statement->execute();
        Message::write("Done", 2);
    }
    private function duplicateArts()
    {
        Message::write("Duplicate HexArticles to PimArticles", 1);
        $connection = $this->em->getConnection();
        $statement = $connection->prepare("
        INSERT pim_articles (category_id, article, ref, art, gamme, type, dim_unitaire, dim_0001, dim_0002, poid_net, poid_unitaire, ean, dos, status, des) 
        SELECT category_id, article, ref, art,  gamme, type, dim_unitaire,  dim_0001, dim_0002, poid_net, poid_unitaire, ean, dos, status, des
        FROM hex_articles;
        ");
        $statement->execute();
        $count = $statement->rowCount();
        Message::write("{$count} lines affected", 2);
    }
    private function useWPData()
    {
        Message::write("Retrive WP JSON", 1);
        $wp_data = file_get_contents($this->wp_json);
        $lang_nodes = json_decode($wp_data);

        $connection = $this->em->getConnection();
        $statement = $connection->prepare("
            SELECT DISTINCT art,dos FROM pim_articles where status = 1
        ");
        $statement->execute();
        $count = $statement->rowCount();
        Message::write("{$count} DISTINCT products found", 3);

        $products = $statement->fetchAll();
        $count = 0;
        $count_complete = 0;
        foreach ($products as $key => $value) {
            $title = $value["art"];
            $dos = $value["dos"];
            $found = 0;
            $statement = $connection->prepare("
                SELECT distinct(hex_fields_i18n.value) as val FROM hex_articles, hex_articles_tree, hex_fields_i18n 
                WHERE hex_articles.category_id = hex_articles_tree.id and hex_articles_tree.id = hex_fields_i18n.tree_label_id 
                AND art = '$title' and hex_fields_i18n.dos = $dos
            ");
            $statement->execute();
            $cat = $statement->fetch();
            $cat = $cat['val'];
            $meta = "";
            $desc = $cat . " - ";
            $offer = $this->locales[$dos];
            foreach ($lang_nodes->$offer as $k => $obj) {
                if ($k == $title || $cat != "" && $obj->reference != "" && strpos($cat, $obj->reference) !== false) {
                    $desc .= $obj->content;
                    $meta = clone $obj;
                    $count_complete++;
                    break;
                }
            }
            unset($meta->content);
            unset($meta->post_title);
            $prod = new PimProducts();
            $prod->setDos($dos);
            $prod->setTitle($title);
            $prod->setRef($title);
            $prod->setDescription($desc);
            $prod->setMetas(serialize($meta));
            $this->em->persist($prod);

            $this->em->flush();
            $this->em->clear();
            $count++;

        }
        Message::write("{$count} products added", 2);
        Message::write("{$count_complete} products completed", 2);
        Message::write("Done", 2);

    }
}
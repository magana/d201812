<?php
namespace App\Hexis\Sync\Incomming\Orchestra;

use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Doctrine\ORM\EntityManager;
use App\Entity\HexArticles;
use App\Hexis\Tools\Message;
use App\Entity\HexFieldsI18n;
use App\Entity\HexArticlesTree;


class OrcHSCA
{
    private $em;
    private $root_data;
    private $file = 'HSCA_WEB.XML';
    
    public function __construct(EntityManager $em, String $root_data)
    {
        $this->em = $em;
        $this->root_data = $root_data;
    }
    public function updateData()
    {
        Message::write("Retriving {$this->file}", 3);
        Message::write("Build subcats table", 4);
        $file = $this->root_data . $this->file;
        $data = file_get_contents($file);

        $serializer = new Serializer([new ObjectNormalizer()], [new XmlEncoder('<Row>')]);
        $cats = $serializer->decode($data, 'xml');
        $repo_tree = $this->em->getRepository(HexArticlesTree::class);
        $repo_i18n= $this->em->getRepository(HexFieldsI18n::class);
        $counter = 0;
        $dos = 100;
        foreach ($cats['Row'] as $key => $row) {
           
                $erpid = str_replace(' ', '', $row['NACDCA'].$row['NACDSC']);
                $cat = $repo_tree->findOneBy(['erp_id' => $erpid]);
                if (!$cat) {
                    $cat = new HexArticlesTree();
                    $cat->setParent($row['NACDCA'])
                ->setErpId($erpid)
                ->setPlace($row['NAORDR']?:0);
                    $this->em->persist($cat);
                }
                $label = $repo_i18n->findOneBy([
                'name' => 'label',
                'dos' => $dos,
                 'tree_label' =>$cat->getId()
            ]);
                if (!$label) {
                    $label = new HexFieldsI18n();
                    $label->setName('label')
                    ->setDos($dos)
                    ->setValue(utf8_decode($row['NALBSC']))
                    ->setTreeLabel($cat)
                    ->setArtLabel(null)
                    ->setRateLabel(null);
                    $this->em->persist($label);
                }
                if ($label || $cat) {
                    $counter ++;
                    $this->em->flush();
                    $this->em->clear();
                }
           
          
        }
        Message::write("$counter lines added", 4);
        Message::write("Done", 3);
  
    }

}
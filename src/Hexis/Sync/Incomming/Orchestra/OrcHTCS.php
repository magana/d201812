<?php
namespace App\Hexis\Sync\Incomming\Orchestra;

use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Doctrine\ORM\EntityManager;
use App\Entity\HexArticles;
use App\Hexis\Tools\Message;
use App\Entity\HexFieldsI18n;
use App\Entity\HexFamilyRates;


class OrcHTCS
{
    private $em;
    private $root_data;
    private $file = 'HTCS_WEB.XML';
    
    public function __construct(EntityManager $em, String $root_data)
    {
        $this->em = $em;
        $this->root_data = $root_data;
    }
    public function updateData()
    {
        Message::write("Retriving {$this->file}", 3);
        Message::write("Build cats table", 4);
        $file = $this->root_data . $this->file;
        $data = file_get_contents($file);

        $serializer = new Serializer([new ObjectNormalizer()], [new XmlEncoder('<Row>')]);
        $rates = $serializer->decode($data, 'xml');
        $repo_rates = $this->em->getRepository(HexFamilyRates::class);
        $repo_i18n= $this->em->getRepository(HexFieldsI18n::class);
        $counter = 0;
        $dos = 100;
        foreach ($rates['Row'] as $key => $row) {
            $rate_save = false;
            $label_save = false;           
                                        
            $rate = $repo_rates->findOneBy(['ref' => $row['NFAMTA']]);
            
            if (!$rate) {                
                $rate = new HexFamilyRates();
                $rate->setErpId($row['NACDCA'].$row['NACDSC'])
                ->setRef($row['NFAMTA']); 
                $this->em->persist($rate);
                $rate_save = true;                       
            }
            $label = $repo_i18n->findOneBy([
                'name' => 'DESIGNATION', 
                'dos' => $dos,
                'rate_label' => $rate->getId()
            ]);
            if (!$label) {
                $des = new HexFieldsI18n();
                $des->setName('DESIGNATION')
                ->setDos($dos)
                ->setValue(utf8_encode($row['NALICO']))
                ->setRateLabel($rate)
                ->setTreeLabel(null)
                ->setArtLabel(null);
                $this->em->persist($des);
            }    
            if($rate_save || $label_save){
                $counter ++;
                $this->em->flush();
                $this->em->clear(); 
            }            
        }
        
        
        Message::write("$counter lines added", 4);
        Message::write("Done", 3);
  
    }

}
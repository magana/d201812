<?php
namespace App\Hexis\Sync\Incomming\Orchestra;

use Doctrine\ORM\EntityManager;
use App\Hexis\Tools\Message;

use App\Sync\Incomming\Orchestra\OrcHSCA;


class OrcFiles
{
    private $em;
    private $root_data;
    private $namespace = 'App\Hexis\Sync\Incomming\Orchestra';
    private $classes;
    
    public function __construct(EntityManager $em, String $root_data)
    {
        $this->em = $em;
        $this->root_data = $root_data;
        $this->classes = [
            '\OrcHCAT',
            '\OrcHSCA',
            '\OrcHTCS',
            '\OrcHART',
        ];
    }
    public function updateData()
    {
        foreach ($this->classes as $class) {
            $this->getData($class);
        }
    }
    private function getData($class){
        $class_name= $this->namespace . $class;
        $data = new $class_name($this->em, $this->root_data );
        $data->updateData();
    }

}
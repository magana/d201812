<?php
namespace App\Hexis\Sync\Incomming\Orchestra;

use App\Hexis\Tools\Message;
use App\Entity\HexArticles;
use Doctrine\ORM\EntityManager;



class OrcStatus
{
    private $em;
    private $repo_arts;
    private $art100;
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repo_arts = $this->em->getRepository(HexArticles::class);
        $this->art100 = $this->get100Data();
        $this->updateData();
    }
    public function updateData()
    {
        $valid_dos = $this->getValidDos();
        foreach ($valid_dos as $dos) {
            $this->updateStatus((int)$dos['dos']);
        }
    }
    private function updateStatus($current_dos)
    {
        $counter = 0;
        $batch_size = 100;
        $str100 = implode(',' ,$this->art100);
        $connection = $this->em->getConnection();
        $statement = $connection->prepare("SELECT id FROM `hex_articles` WHERE DOS={$current_dos} AND article IN ($str100)");
        $statement->execute();
        $current_arts = $statement->fetchAll();
        $counter = count($current_arts);
        $str_current_dos = [];
        foreach ($current_arts as $ids) {
            $str_current_dos[] = $ids['id'];
        }
        $str_current_dos = implode(',',$str_current_dos);
        $statement = $connection->prepare("UPDATE `hex_articles` SET `status` = '1' WHERE `hex_articles`.`id` in ({$str_current_dos})");
        $statement->execute();
        Message::write("DOS:$current_dos - $counter status updated", 3);
    }
    private function get100Data()
    {
        $connection = $this->em->getConnection();
        $statement = $connection->prepare("SELECT article FROM `hex_articles` WHERE `dos`=100 AND `status`=1");
        $statement->execute();
        $dos_arts = $statement->fetchAll();
        $arts=[];
        foreach ($dos_arts as $art) {
            $arts[] = "'".$art['article']."'";
        }
        return $arts;
    }
    private function getValidDos()
    {
        $art_dos = $this->repo_arts->getNbArtsByDos();
        $langs = [
            '200' => 'German',
            '300' => 'Italian',
            '400' => 'Spanish',
            '500' => 'Sweden'
        ];
        foreach ($art_dos as $key => $dos) {
            if ($dos['dos']!=100 && $dos['dos']!=120 && $dos['dos']!= 'all') {
                $art_dos[$key]['lang'] = $langs[$dos['dos']];
            } else {
                unset($art_dos[$key]);
            }
        }
        return $art_dos;
    }

}
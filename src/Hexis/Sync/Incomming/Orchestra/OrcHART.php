<?php
namespace App\Hexis\Sync\Incomming\Orchestra;

use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Doctrine\ORM\EntityManager;
use App\Entity\HexArticles;
use App\Hexis\Tools\Message;
use App\Entity\HexFieldsI18n;
use App\Entity\HexFamilyRates;
use App\Entity\HexArticlesTree;
use DateTime;


class OrcHART
{
    private $em;
    private $root_data;
    private $files = [
        [ 'file' => 'HART_WFR.XML', 'dos' => 100 ],
        [ 'file' => 'HART_WDE.XML', 'dos' => 200 ],
        [ 'file' => 'HART_WSW.XML', 'dos' => 500 ],
    ];

    public function __construct(EntityManager $em, String $root_data)
    {
        $this->em = $em;
        $this->root_data = $root_data;
    }
    public function updateData()
    {
        foreach ($this->files as $file) {
            Message::write("Retriving {$file['file']}", 3);
            Message::write("Build Arts table", 4);
            $file_uri = $this->root_data . $file['file'];
            $data = file_get_contents($file_uri);

            $serializer = new Serializer([new ObjectNormalizer()], [new XmlEncoder()]);
            $arts = $serializer->decode($data, 'xml');
            $repo_tree = $this->em->getRepository(HexArticlesTree::class);
            $repo_rates = $this->em->getRepository(HexFamilyRates::class);
            $repo_i18n = $this->em->getRepository(HexFieldsI18n::class);
            $counter = 0;
            $dos = 100;
            $batch_size = 500;
            foreach ($arts['Row'] as $key => $row) {
                $rate_save = false;
                $label_save = false;
                if (isset($row['NSADES'])) {
                    $counter++;
                    $dos = $file['dos'];
                    $ref = $row['NCARTI'] . $row['NCSSAR'];
                    $ncarti = $row['NCARTI'];
                    $ncssar = $row['NCSSAR'];


                    $rate = $repo_rates->findOneBy(['ref' => $row['NFAMTA']]);
                    $game = "";
                    $tree_el = null;
                    if ($rate) {
                        $rate_des = $repo_i18n->findOneBy([
                        'rate_label' => $rate->getId(),
                        'dos' => $dos
                    ]);
                        if ($rate_des) {
                            $rate_ref = $rate->getRef();
                            preg_match('/\sG(?<game>[0-9]+)\s/', $rate_des->getValue(), $matches);
                            $game = ($matches) ? $game = $matches['game'] : 0;
                        }
                        $tree_el = $repo_tree->findOneBy([
                        'erp_id' => $rate->getErpId(),
                    ]);
                    }
                    $status = 0;
                    if($dos==100) $status = 1;
                    if (strpos($ncssar, 'HX20') === 0 && substr($ncssar, -1) == "P") {
                        $status = 0;
                    }
                    $art = new HexArticles();
                    $art->setArticle($ref)
                    ->setDos($dos)
                    ->setRef($ncarti)
                    ->setArt($ncssar)
                    ->setStatus($status)
                    ->setDateCreated(new DateTime("now"))
                    ->setDateUpdated(new DateTime("now"))
                    ->setGamme($game)
                    ->setType($row['NATYPE'])
                    ->setDim0001($row['NSALON'])
                    ->setDim0002($row['NSALAR'])
                    ->setPoidUnitaire('')
                    ->setPoidBrut('')
                    ->setPoidNet($row['NSAPDN'])
                    ->setEan('')
                    ->setDimUnitaire('')
                    ->setDes(( $row['NSADES'] ))
                    ->setFamilyRate($rate);


                    if ($tree_el) {
                        $art->setCategory($tree_el);
                    } else {
                        $art->setCategory(null);
                    }
                    /**/
                    $this->em->persist($art);



                    if (($counter % $batch_size) === 0) {
                        $this->em->flush();
                        $this->em->clear();
                    }
                    if (($counter % 1000) == 0) {
                        //Message::write("$counter lines added", 5);
                    }
                }
                if ($rate_save || $label_save) {
                    $counter++;
                    $this->em->flush();
                    $this->em->clear();
                }
            }


            Message::write("$counter lines added", 5);
            Message::write("Done", 3);
        }
    }

}
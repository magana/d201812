<?php

namespace App\Repository;

use App\Entity\PimArticles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PimArticles|null find($id, $lockMode = null, $lockVersion = null)
 * @method PimArticles|null findOneBy(array $criteria, array $orderBy = null)
 * @method PimArticles[]    findAll()
 * @method PimArticles[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PimArticlesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PimArticles::class);
    }


    // /**
    //  * @return PimArticles[] Returns an array of PimArticles objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PimArticles
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
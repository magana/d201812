<?php

namespace App\Repository;

use App\Entity\HexArticlesTree;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\Query\Expr;

/**
 * @method HexArticlesTree|null find($id, $lockMode = null, $lockVersion = null)
 * @method HexArticlesTree|null findOneBy(array $criteria, array $orderBy = null)
 * @method HexArticlesTree[]    findAll()
 * @method HexArticlesTree[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HexArticlesTreeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, HexArticlesTree::class);
    }
    public function getNbTreeByDos()
    {
        $em = $this->getEntityManager();
        $connection = $em->getConnection();
        $sql  = "SELECT count(*) AS results, i18n.dos FROM `hex_articles_tree` tree 
        LEFT JOIN hex_fields_i18n i18n 
        ON i18n.tree_label_id = tree.id 
        WHERE i18n.tree_label_id IS NOT NULL 
        GROUP BY i18n.dos";
        $statement = $connection->prepare($sql);
        $statement->execute();
        $art_dos = $statement->fetchAll();         
        $all_dos = ['results' => 0, 'dos' =>'all'];
        foreach ($art_dos as $dos) {
            $all_dos['results'] += $dos['results'];
        }
       
        $art_dos[] = $all_dos;
        return $art_dos;        
    }
    public function getTreeWithDos($dos)
    {
        if($dos=='all'){
            $em = $this->getEntityManager();
            $connection = $em->getConnection();
            $sql  = "SELECT * FROM `hex_articles_tree` tree 
            LEFT JOIN hex_fields_i18n i18n 
            ON i18n.tree_label_id = tree.id 
            WHERE i18n.tree_label_id IS NOT NULL";
            $statement = $connection->prepare($sql);
            $statement->execute();
            $art_dos = $statement->fetchAll();  
            return $art_dos;
        }else{
            $qr = $this->createQueryBuilder('tree')
            ->leftJoin('App\Entity\HexFieldsI18n', 'i18n', 'WITH','i18n.tree_label = tree.id')
            ->andWhere("i18n.dos=:dos")
            ->andWhere('i18n.tree_label IS NOT NULL')
            ->setParameter('dos', $dos)
            ->getQuery();
            return $qr->getResult();
        }        
    }
    public function getGammeAndParent($dos)
    {
        $qr = $this->createQueryBuilder('t')
        ->select('DISTINCT t.erp_id', 'a.gamme')
        ->leftJoin('App\Entity\HexArticles', 'a', 'WITH','a.gamme > 0 AND t.id=a.category')
        ->andWhere("a.dos=:dos")
        ->setParameter('dos', $dos)
        ->getQuery();        
        return $qr->getResult();
    }
    public function getFirstLevelByDos($dos)
    {
        $qr = $this->createQueryBuilder('t')
        ->select('t.id, t.erp_id, i18n.value AS title, i18n.dos')
        ->leftJoin('App\Entity\HexFieldsI18n', 'i18n', 'WITH','t.id = i18n.tree_label AND i18n.dos = :dos')
        ->andWhere('t.parent IS NULL')
        ->setParameter('dos', $dos)
        ->getQuery();
        return $qr->getResult();
    }
    public function getSecondLevelByDos($parent, $dos)
    {
        $qr = $this->createQueryBuilder('t')
        ->select('t.id, t.erp_id, i18n.value AS title, i18n.dos')
        ->innerJoin('App\Entity\HexArticlesTree', 't2', 'WITH','t.parent = :parent')
        ->leftJoin('App\Entity\HexFieldsI18n', 'i18n', 'WITH','t.id = i18n.tree_label AND i18n.dos = :dos')
        ->groupBy('t.id')
        ->setParameter('dos', $dos)
        ->setParameter('parent', $parent)
        ->getQuery();        
        return $qr->getResult();
    }
}
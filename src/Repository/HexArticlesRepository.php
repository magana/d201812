<?php

namespace App\Repository;

use App\Entity\HexArticles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\Query\Expr\Join;

/**
 * @method HexArticles|null find($id, $lockMode = null, $lockVersion = null)
 * @method HexArticles|null findOneBy(array $criteria, array $orderBy = null)
 * @method HexArticles[]    findAll()
 * @method HexArticles[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HexArticlesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, HexArticles::class);
    }
    public function getNbArtsByDos(){
        $em = $this->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT count(*) AS results, DOS AS dos FROM `hex_articles` GROUP BY DOS HAVING COUNT(*) > 1");
        $statement->execute();
        $art_dos = $statement->fetchAll();         
        $all_dos = ['results' => 0, 'dos' =>'all'];
        foreach ($art_dos as $dos) {
            $all_dos['results'] += $dos['results'];
        }
        $art_dos[] = $all_dos;
        return $art_dos;        
    }
    public function getArtsWithTree($dos)
    {
        if($dos=='all'){

        }
        $qr = $this->createQueryBuilder('a')
        ->select('i2.value AS TreeName1, i.value as TreeName2, a.gamme as Gamme, a.ref as CodeProduit, a.des, a.article, a.art, t.erp_id, a.id')
        ->leftJoin('App\Entity\HexArticlesTree', 't', 'WITH','t.id = a.category')
        ->innerJoin('App\Entity\HexFieldsI18n', 'i', 'WITH', 'i.tree_label = t.id and i.dos = :dos')
        ->innerJoin('App\Entity\HexArticlesTree', 't2', 'WITH', 't.parent = t2.erp_id')
        ->innerJoin('App\Entity\HexFieldsI18n', 'i2', 'WITH', 'i2.tree_label = t2.id and i2.dos = :dos')
        ->where("a.dos=:dos")
        ->orderBy('a.gamme', 'ASC')
        ->setParameter('dos', $dos)
        ->getQuery();
        return $qr->getResult();
    }
    public function getGameLevelByDos($parent)
    {
        $qr = $this->createQueryBuilder('a')
        ->select('a.gamme')
        ->innerJoin('App\Entity\HexArticlesTree', 't', 'WITH','t.erp_id = :parent AND a.category=t.id')
        ->orderBy('a.gamme', 'ASC')
        ->groupBy('a.gamme')
         ->setParameter('parent', $parent)
        ->getQuery();        
        return $qr->getResult();
    }
    public function getArtByCriteria($dos, $erp_id=null, $gamme=null)
    {
        if($erp_id=='0' && $gamme=='null'){
            $qr = $this->createQueryBuilder('a')
            ->where('a.dos = :dos AND a.status=1')
            ->setParameter('dos', $dos)
            ->groupBy('a.art')
            ->orderBy('a.gamme', 'ASC')
            ->getQuery();
            return $qr->getResult();
        }
        if($erp_id && $gamme=='null'){
            $erp_id = $erp_id . "%";
            $qr = $this->createQueryBuilder('a')
                ->innerJoin('App\Entity\HexArticlesTree', 't', 'WITH', 't.erp_id like :erp_id AND a.category=t.id AND a.status=1')
                ->groupBy('a.art')
                ->orderBy('a.gamme', 'ASC')
                ->where("a.dos=:dos")
                ->setParameter('erp_id', $erp_id)
                ->setParameter('dos', $dos)
                ->getQuery();
            return $qr->getResult();
        }
        $erp_id = $erp_id . "%";
        $qr = $this->createQueryBuilder('a')
            ->innerJoin('App\Entity\HexArticlesTree', 't', 'WITH', 't.erp_id like :erp_id AND a.category=t.id AND a.status=1')
            ->where('a.gamme = :gamme')
            ->andWhere("a.dos = :dos")
            ->groupBy('a.art')
            ->orderBy('a.gamme', 'ASC')
            ->setParameter('erp_id', $erp_id)
            ->setParameter('dos', $dos)
            ->setParameter('gamme', $gamme)
            ->getQuery();
            return $qr->getResult(); 
     
        return $qr->getResult();
    }
}
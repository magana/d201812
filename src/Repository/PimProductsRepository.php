<?php

namespace App\Repository;

use App\Entity\PimProducts;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PimProducts|null find($id, $lockMode = null, $lockVersion = null)
 * @method PimProducts|null findOneBy(array $criteria, array $orderBy = null)
 * @method PimProducts[]    findAll()
 * @method PimProducts[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PimProductsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PimProducts::class);
    }

    // /**
    //  * @return PimProducts[] Returns an array of PimProducts objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PimProducts
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

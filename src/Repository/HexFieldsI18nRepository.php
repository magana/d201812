<?php

namespace App\Repository;

use App\Entity\HexFieldsI18n;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method HexFieldsI18n|null find($id, $lockMode = null, $lockVersion = null)
 * @method HexFieldsI18n|null findOneBy(array $criteria, array $orderBy = null)
 * @method HexFieldsI18n[]    findAll()
 * @method HexFieldsI18n[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HexFieldsI18nRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, HexFieldsI18n::class);
    }
}
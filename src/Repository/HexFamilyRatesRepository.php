<?php

namespace App\Repository;

use App\Entity\HexFamilyRates;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method HexFamilyRates|null find($id, $lockMode = null, $lockVersion = null)
 * @method HexFamilyRates|null findOneBy(array $criteria, array $orderBy = null)
 * @method HexFamilyRates[]    findAll()
 * @method HexFamilyRates[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HexFamilyRatesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, HexFamilyRates::class);
    }
    
    public function getNbRateByDos()
    {
        $em = $this->getEntityManager();
        $connection = $em->getConnection();
        $sql = "SELECT count(*) AS results, i18n.dos FROM `hex_family_rates` rates 
        LEFT JOIN hex_fields_i18n i18n 
        ON i18n.rate_label_id = rates.id 
        WHERE i18n.rate_label_id IS NOT NULL 
        GROUP BY i18n.dos";
        $statement = $connection->prepare($sql);
        $statement->execute();
        $cats_dos = $statement->fetchAll();
        $all_dos = ['results' => 0, 'dos' =>'all'];
        foreach ($cats_dos as $dos) {
            $all_dos['results'] += $dos['results'];
        }
        $cats_dos[] = $all_dos;  
        return $cats_dos;  
    }
    public function getRatesWithDos($dos)
    {
        if($dos=='all'){
            $em = $this->getEntityManager();
            $connection = $em->getConnection();
            $sql  = "SELECT * FROM `hex_family_rates` rate 
            LEFT JOIN hex_fields_i18n i18n 
            ON i18n.rate_label_id = rate.id 
            WHERE i18n.rate_label_id IS NOT NULL";
            $statement = $connection->prepare($sql);
            $statement->execute();
            $art_dos = $statement->fetchAll();  
            return $art_dos;
        }else{
            $qr = $this->createQueryBuilder('rates')
            ->leftJoin('App\Entity\HexFieldsI18n', 'i18n', 'WITH','i18n.rate_label = rates.id')
            ->andWhere("i18n.dos=:dos")
            ->andWhere('i18n.rate_label IS NOT NULL')
            ->setParameter('dos', $dos)
            ->getQuery();
            return $qr->getResult();
        } 
    }  

}
// webpack v4
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin')
var babelenv = require('babel-preset-env');
const webpack = require('webpack');

module.exports = {
  devtool: "source-map",
  mode: 'production',
  entry: {
    main: "./assets/index.js",
    explorecss: ["./assets/explore.js"],
    muheader: "./assets/scss/mu-header-styles.js",
    catalogue: "./assets/js/catalogue.js",
    app: "./assets/js/app.js",
  },

  output: {
    path: path.resolve(__dirname, "public/assets"),
    filename: "js/[name].js",
    publicPath: "/assets/"
  },


  module: {
    rules: [{
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [babelenv]
          }
        }
      },
      {
        test: /\.s?[c|a]ss$/,
        use: [
          "style-loader",
          MiniCssExtractPlugin.loader,
          "css-loader",
          "postcss-loader",
          "sass-loader"
        ]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin("public/assets/", {}),
    new MiniCssExtractPlugin({
      filename: "css/[name].css"
    }),
    new CopyWebpackPlugin(
      [{
        from: './node_modules/lazyloadxt/dist/jquery.lazyloadxt.js',
        to: 'js/libs/lazyloadxt/',
        toType: 'dir'
      }, {
        from: './node_modules/bootstrap/dist/js/bootstrap.min.js',
        to: 'js/libs/bootstrap/',
        toType: 'dir'
      }, {
        from: './node_modules/popper.js/dist/umd/popper.min.js',
        to: 'js/libs/popper.js/',
        toType: 'dir'
      }, {
        from: './node_modules/js-cookie/src/js.cookie.js',
        to: 'js/libs/js-cookie/',
        toType: 'dir'
      }, {
        from: './node_modules/jqueryrouter/dist/js/jquery.router.min.js',
        to: 'js/libs/jqueryrouter/',
        toType: 'dir'
      }, {
        from: './node_modules/ekko-lightbox/dist/ekko-lightbox.min.js',
        to: 'js/libs/ekko-lightbox/',
        toType: 'dir'
      }, {
        from: './node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js',
        to: 'js/libs/datatables/',
        toType: 'dir'
      }, {
        from: './node_modules/jquery/dist/jquery.min.js',
        to: 'js/libs/jquery/',
        toType: 'dir'
      }, {
        from: './assets/js/libs/iframe-lightbox/iframe-lightbox.js',
        to: 'js/libs/iframe-lightbox/',
        toType: 'dir'
      }, {
        from: './assets/js/libs/iframe-lightbox/iframe-lightbox.css',
        to: 'js/libs/iframe-lightbox/',
        toType: 'dir'
      }, {
        from: './assets/js/raw/',
        to: 'js/shared/'
      }, {
        from: './assets/css/fontello-embedded.css',
        to: 'css/'
      }, {
        from: './assets/img/',
        to: 'img/'
      }, {
        from: './assets/favicon.ico',
        to: ''
      }], {
        copyUnmodified: true
      }),
    new HtmlWebpackPlugin({
      inject: false,
      hash: true,
      template: "./templates/base-template.html.twig",
      filename: __dirname + "/templates/base.html.twig"
    })
  ]
};